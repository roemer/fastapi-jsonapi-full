# Changelog

## v0.1.32

 -  Add case-insensitive comparison operator `ieq`

## v0.1.31

 -  Verify id in request path against model

## v0.1.30

 - Fix problems when using a multi-step filter

## v0.1.29

 - Fix proper counts on included entities

## v0.1.28

 - Fix proper counts on last page of related requests

## v0.1.27

 - Fix proper aliasing when using self-joins 

## v0.1.26

 - Only do count queries when needed. If possible, infer from data query.
 - Allow per-include setting of limits using :n syntax

## v0.1.25

 - Implicit (lazy) joins
 - Do not allow direct usage of to-many relations for filtering and sorting

## v0.1.24

 - Better exception for wrong filters 

## v0.1.23

 - Better exception for wrong filters 

## v0.1.22

 - Handle casing in lookups for 'any' filters

## v0.1.21

 - Remove 'DISTINCT' (unique) from SqlAlchemy queries (performance!)

## v0.1.20

 - Add `any` operation to allow filtering though to-many relations
 - Test data for many-to-many relations

## v0.1.19

 - Fix slow schema generation - finally  

## v0.1.18

 - Fix slow schema generation - really - through bypassing Pydantic for JsonAPI envelope schema generation.  

## v0.1.17

 - Fix slow schema generation after Pydantic 2.6 dependency update using parallel schema generation. 

## v0.1.16

 - Mitigate slow schema generation after Pydantic 2.6 dependency update 
 - Fix to allow additional fields from queries;    
   
   The additional fields will be ignored. This is useful if you need
   to use DISTINCT and there are fields on which you want to order.
   
   It is assumed that the first set of fields are the object that
   should be returned.

## v0.1.15

 - Fix filters after Pydantic 2.6 dependency update

## v0.1.14

 - Dependency update: Pydantic 2.6

## v0.1.13

 - Dependency update: Starlette, Fastapi, Httpx

## v0.1.12

 - Refactor base query, move to SqlAlchemy 2.0 style queries
 
## v0.1.11

 - Handle exceptions on non-existing attributes for filters/ sorts (#23)

## v0.1.10

 - Fix updates on attributes of type set

## v0.1.9

 - Async support (#10)

## v0.1.8

 - Add default sort to the baseDataLayer (#21)
 - Fix: page[limit]=0 leads to ZeroDivisionError: division by zero (#20)
 - Add tests for pagination, sort, filtering on related items/ relationships

## v0.1.7

 - Add SqlAlchemy BaseDataLayer

## v0.1.6

 - Allow default filter and sort parameters for GET of related items and relationships

## v0.1.5

 - Check if BaseDataLayer.orm_type is not None, do not use "truthy" comparison (to allow for non-truthy orm_types)

## v0.1.4

 - Return clear error message when supplying wrong filter parameters (#19)

## v0.1.3
 
 - Allow sets as a collection class in the BaseDataLayer  

## v0.1.2

 - Add optional request_class param to JsonAPIRouter add_route methods

## v0.1.1

 - Move generic utility functions to utils.py module

## v0.1.0

 - Refactoring: use standard FastAPI dependency injection for JsonAPI routes
 - Implement JsonAPI Requests class to be used as dependency
 - Move Resource classes to handler functions

## v0.0.35

 - Register relationship_delete resource with a single object schema
 
## v0.0.34

 - Rework SqlAlchemy filter & sort params to return clauses. 
   These clauses can be applied multiple times, for instance when working with CTE expressions.
   That, in turn, is needed for filtering/ sorting on related entities. 

## v0.0.33

 - Fix: Pagination offset is wrongly applied to the included relationships (#18)

## v0.0.32

 - Fix: "?include=" fails on whitespace (#17)

## v0.0.31
 
 - Documentation of filtering, sorting query parameters

## v0.0.30
 
 - Add filtering, sorting on related entities (#16)

## v0.0.29

 - Fix: Increase in pagination limit applies to included

## v0.0.28

 - Fix: Pass kwargs into before_create hook (#15)

## v0.0.27

 - Fix: OpenAPI schema was not registered for register_related_get

## v0.0.26

 - Create `before_create` and `before_save` hooks for `POST` and `related_POST` endpoints

## v0.0.25

 - Change the "included" behavior for related endpoints: take the related item as the reference for the include argument

## v0.0.24
 - Create specific methods for registering endpoints, so that fine-grained overrides are possible
 - Rename endpoints in line with HTTP methods, eg. GET, POST, PATCH, DELETE 

## v0.0.23
 - Implement POST for relationships
 - Remove "Exception" suffix for all known exceptions (naming convention)

## v0.0.22
 - Separate meta-info for entities and relationships by using naming convention '_relationships'

## v0.0.21
 - Add warning for unexpected 'meta' data

## v0.0.20
 - Pass on meta-information on relationships

## v0.0.19
 - Add reference to model in data layer

## v0.0.18
 - Support for meta fields with Schema information
 - Use named column for foreign keys for processing related objects 
 - Add remote_side to Relationship config for easier querying of relations
 - Fix: correctly pass relationship to ItemNotFound exception
 - Changelog added
 - Readme added
 
