import pytest


JSONAPI_ENDPOINT_RELATIONSHIPS = {
    'persons': ['organization'],
    'organizations': ['organization-members'],
}


def jsonapi_endpoint_relationship_pairs():
    for endpoint, relationships in JSONAPI_ENDPOINT_RELATIONSHIPS.items():
        for relationship in relationships:
            yield endpoint, relationship


@pytest.mark.parametrize('endpoint', JSONAPI_ENDPOINT_RELATIONSHIPS)
def test_endpoint_get(client, endpoint):
    result = client.get(endpoint)
    assert result.status_code == 200


@pytest.mark.parametrize('endpoint, include', jsonapi_endpoint_relationship_pairs())
def test_endpoint_get_with_includes(client, endpoint, include):
    url = f"{endpoint}?include={include}"
    result = client.get(url)
    assert result.status_code == 200
    data = result.json()

