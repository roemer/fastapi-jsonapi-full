import pytest
from fastapi_jsonapi_full import JsonAPIEnvelope
import fastapi_jsonapi_full.exceptions as ex

from .utils import get_json_result


def test_person_patch_relationship_simple(client):
    patch = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization', json=patch))
    assert result == {
        'data': {
            'type': 'Organization',
            'id': '2'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_person_patch_relationship_with_include(client):
    patch = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization?include=organization', json=patch))
    assert result == {
        'data': {
            'type': 'Organization',
            'id': '2'
        },
        'included': [{'attributes': {'name': 'National Trust'},
                      'id': '2',
                      'links': {'self': '/organizations/2'},
                      'relationships': {'organization-members': {'links': {'related': '/organizations/2/organization-members',
                                                              'self': '/organizations/2/relationships/organization-members'}}},
                      'type': 'Organization'}],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization',
        }
    }


def test_person_patch_relationship_none(client):
    patch = {
        'data': None
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization', json=patch))
    assert result == {
        'data': None,
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_person_patch_relationship_empty(client):
    patch = {
        'data': []
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization', json=patch), status_code=422)


def test_person_patch_relationship_many(client):
    patch = {
        'data': [{
                'type': 'Organization',
                'id': '1'
            },
            {
                'type': 'Organization',
                'id': '2'
            }
        ]
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization', json=patch), status_code=422)


def test_person_patch_relationship_none_with_include(client):
    patch = {
        'data': None
    }
    result = get_json_result(client.patch('/persons/1/relationships/organization?include=organization', json=patch))
    assert result == {
        'data': None,
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_organization_patch_relationship_simple(client):
    patch = {
        'data': [{
            'type': 'Person',
            'id': '2'
        }]
    }
    result = get_json_result(client.patch('/organizations/1/relationships/organization-members', json=patch))
    assert result == {
        'data': [{
            'type': 'Person',
            'id': '2'
        }],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/relationships/organization-members',
            'related': '/organizations/1/organization-members'
        }
    }


def test_organization_patch_relationship_empty(client):
    patch = {
        'data': []
    }
    result = get_json_result(client.patch('/organizations/1/relationships/organization-members', json=patch))
    assert result == {
        'data': [],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/relationships/organization-members',
            'related': '/organizations/1/organization-members'
        }
    }


def test_organization_patch_relationship_many(client):
    patch = {
        'data': [
            {
            'type': 'Person',
            'id': '1'
            },
            {
                'type': 'Person',
                'id': '3'
            }
        ]
    }
    result = get_json_result(client.patch('/organizations/1/relationships/organization-members', json=patch))
    assert result == {
        'data': [{
            'type': 'Person',
            'id': '1'
            },
            {
                'type': 'Person',
                'id': '3'
            }
        ],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/relationships/organization-members',
            'related': '/organizations/1/organization-members'
        }
    }


def test_organization_patch_relationship_none(client):
    patch = {
        'data': None
    }
    result = get_json_result(client.patch('/organizations/1/relationships/organization-members', json=patch), status_code=422)


# @pytest.mark.xfail
# def test_person_patch_relationship_empty(client):
#     patch = {
#         'data': {
#             'attributes': {'firstName': 'Johnny'},
#             'id': '1',
#             'type': 'Person',
#             'relationships': {
#                 'organization': {
#                     'data': []
#                 }
#             },
#         }
#     }
#     result = get_json_result(client.patch('/persons/1?include=organization', json=patch), status_code=422)
#
#     assert result == {
#         'errors': [{
#             'code': 'UnexpectedContent',
#             'detail': "Expected 'object', but found '[]' for relation 'Person.organization'",
#             'status': '422',
#             'title': 'Unexpected content'
#         }],
#         'jsonapi': {'version': '1.0'}
#     }
