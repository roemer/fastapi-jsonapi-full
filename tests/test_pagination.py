import pytest


@pytest.fixture(scope='function')
def _create_persons(client):
    # create 10 persons each for organization 1, organization 2
    for i in range(1, 10):
        _create_person(client, f'Jan {i} Organization 1', 1)
        _create_person(client, f'Jan {i} Organization 2', 2)


def _create_person(client, name: str, organization_id: int) -> str:
    data = {
        'data': {
            'attributes': {'firstName': name, 'lastName': 'De Vries'},
            'type': 'Person'
        }
    }
    result = client.post(
        f'/organizations/{organization_id}/organization-members',
        json=data
    )

    assert result.status_code == 201
    data = result.json()

    # retrieve latest product by id
    product_id = data['data']['id']
    return product_id


@pytest.mark.parametrize('limit,offset,count', (
        (1, 0, 1),
        (10, 0, 10),
        (10, 10, 10),
        (10, 20, 2),
        (0, 0, 0),
        (0, 0, 0),
))
def test_pagination(client, _create_persons, limit, offset, count):
    # retrieve participations 1, 2 with include
    result = client.get(f'/persons?page[limit]={limit}&page[offset]={offset}')

    assert result.status_code == 200
    data = result.json()

    assert len(data['data']) == count

    assert data['meta']['count'] == 22


@pytest.mark.parametrize('limit,count', (
        (10, 20),
        (2, 4),
))
def test_pagination_for_includes(client, _create_persons, limit, count):
    # retrieve participations 1, 2 with include
    result = client.get(f'/organizations?mode=edit&filter=[{{"name":"id", "op": "in", "val": [1, 2] }}]&include=organization-members:{limit}&page[limit]={limit}')

    assert result.status_code == 200, result.text
    data = result.json()
    included = data['included']

    assert len(data['data']) == 2

    # there should only be Products in the included list
    assert all(item['type'] == 'Person' for item in included)

    # length of the included list should be 20 (10 Products for each Participation)
    assert len(included) == count


@pytest.mark.parametrize('includes,limit,count_persons,count_hobbies', (
        ('organization-members', 1, 10, 0),
        ('organization-members:3', 1, 3, 0),
        ('organization-members:3.hobbies:1', 1, 3, 2),
        ('organization-members:1.hobbies:1', 2, 2, 1),
        ('organization-members:3,organization-members.hobbies:1', 1, 3, 2),

))
def test_pagination_limit_for_includes(client, _create_persons, includes, limit, count_persons, count_hobbies):
    # retrieve participations 1, 2 with include
    result = client.get(f'/organizations?mode=edit&include={includes}&page[limit]={limit}')

    assert result.status_code == 200, result.text
    data = result.json()
    included_hobbies = [i for i in data['included'] if i['type'] == 'Hobby']
    included_persons = [i for i in data['included'] if i['type'] == 'Person']

    assert len(included_persons) == count_persons
    assert len(included_hobbies) == count_hobbies
    # length of the included list should be 20 (10 Products for each Participation)

def test_pagination_limit_for_includes_links(client, _create_persons):
    result = client.get(f'/organizations?mode=edit&include=organization-members.hobbies:3&page[limit]=1')

    assert result.status_code == 200, result.text
    data = result.json()

    # Link should use the same limit settings
    for link in ['first', 'next','last']:
        assert data['links'][link].startswith('/organizations?mode=edit&include=organization-members.hobbies%3A3')


def test_pagination_include_multistep(client, _create_persons):
    # when we sprecify a second level relation as include, both are included
    # and both can be limited seperately
    result = client.get(f'/organizations?mode=edit'
                        f'&include=organization-members:3.hobbies:1'
                        f'&page[limit]=1')
    assert result.status_code == 200, result.text
    data = result.json()
    included_hobbies = [i for i in data['included'] if i['type'] == 'Hobby']
    included_persons = [i for i in data['included'] if i['type'] == 'Person']
    assert len(included_persons) == 3
    assert len(included_hobbies) == 2 # 1 for each person

@pytest.mark.parametrize('endpoint,limit,count', (
        ('/organizations/1/organization-members', 10, 10),
        ('/organizations/1/organization-members', 2, 2),
        ('/organizations/1/relationships/organization-members', 10, 10),
        ('/organizations/1/relationships/organization-members', 2, 2)
))
def test_pagination_for_related(client, _create_persons, endpoint, limit, count):
    # retrieve participations 1, 2 with include
    result = client.get(f'{endpoint}?page[limit]={limit}')

    assert result.status_code == 200
    data = result.json()
    items = data['data']
    assert data.get('included') is None
    total_count = data['meta']['count']

    assert len(items) == count

    # there should only be Persons in the included list
    assert all(item['type'] == 'Person' for item in items)

    # get last page
    offset = int(total_count / limit) * limit
    result = client.get(f'{endpoint}?page[limit]={limit}&page[offset]={offset}')
    assert result.status_code == 200
    data = result.json()
    assert data['meta']['count'] == total_count




@pytest.mark.parametrize('endpoint,first_sort_id', (
        ('/organizations/1/organization-members?sort=id', '1'),
        ('/organizations/1/organization-members?sort=id&page[limit]=1&page[offset]=0', '1'),
        ('/organizations/1/organization-members?sort=id&page[limit]=1&page[offset]=1', '2'),
        ('/organizations/1/organization-members?sort=id&page[limit]=1&page[offset]=10', '21'),
        ('/organizations/1/organization-members?sort=-id', '21'),
        ('/organizations/1/organization-members?sort=-id&page[limit]=1&page[offset]=0', '21'),
        ('/organizations/1/organization-members?sort=-id&page[limit]=1&page[offset]=1', '19'),
        ('/organizations/1/organization-members?sort=-id&page[limit]=1&page[offset]=10', '1'),
        ('/organizations/1/relationships/organization-members?sort=id', '1'),
        ('/organizations/1/relationships/organization-members?sort=id&page[limit]=1&page[offset]=0', '1'),
        ('/organizations/1/relationships/organization-members?sort=id&page[limit]=1&page[offset]=1', '2'),
        ('/organizations/1/relationships/organization-members?sort=id&page[limit]=1&page[offset]=10', '21'),
        ('/organizations/1/relationships/organization-members?sort=-id', '21'),
        ('/organizations/1/relationships/organization-members?sort=-id&page[limit]=1&page[offset]=0', '21'),
        ('/organizations/1/relationships/organization-members?sort=-id&page[limit]=1&page[offset]=1', '19'),
        ('/organizations/1/relationships/organization-members?sort=-id&page[limit]=1&page[offset]=10', '1'),
))
def test_pagination_for_related_sorted(client, _create_persons, endpoint, first_sort_id):
    result = client.get(f'{endpoint}')

    assert result.status_code == 200
    data = result.json()
    items = data['data']
    assert data.get('included') is None

    # check the id of the FIRST item in the list
    assert items[0]['id'] == first_sort_id


@pytest.mark.parametrize('endpoint,first_sort_id,count', (
        ('/organizations/1/organization-members?filter[id]=1', '1', 1),
        ('/organizations/1/relationships/organization-members?filter[id]=1', '1', 1),
))
def test_pagination_for_related_filtered(client, _create_persons, endpoint, first_sort_id, count):
    result = client.get(f'{endpoint}')

    assert result.status_code == 200
    data = result.json()
    items = data['data']
    assert data.get('included') is None

    # check the id of the FIRST item in the list
    assert items[0]['id'] == first_sort_id

    assert data['meta']['count'] == count
