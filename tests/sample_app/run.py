from sample_app.app import create_app, create_db, run

if __name__ == '__main__':
    create_db()
    run(create_app(), host="0.0.0.0", port=8000)
