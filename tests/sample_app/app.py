from fastapi import FastAPI, Depends
from fastapi.openapi.utils import get_openapi
from uvicorn import run
import logging

from fastapi_jsonapi_full import register_jsonapi_schema_components, JsonAPIRouter, BaseModel

from .handlers import (
    delete,
    get_many,
    get,
    patch,
    post,
    related_get,
    related_post,
    relationship_delete,
    relationship_get,
    relationship_patch,
    relationship_post,
)
from .storage import create_db, get_session
from .data_layer import PersonDataLayer, OrganizationDataLayer, HobbyDataLayer, NodeDataLayer
from .models import Person, Organization, Hobby, Node

logger = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')


def create_app():
    app = FastAPI()

    BaseModel.jsonapi_init_schemas()

    # prepare the data layer by registering the right session creation function
    person_resource = JsonAPIRouter(
        'persons',
        Person,
        PersonDataLayer(),
        tags=['persons']
    )
    organization_resource = JsonAPIRouter(
        'organizations',
        Organization,
        OrganizationDataLayer(),
        tags=['organizations']
    )
    hobby_resource = JsonAPIRouter(
        'hobbies',
        Hobby,
        HobbyDataLayer(),
        tags=['hobbies']
    )
    node_resource = JsonAPIRouter(
        'nodes',
        Node,
        NodeDataLayer(),
        tags=['nodes']
    )


    person_resource.add_jsonapi_delete_route(delete)
    person_resource.add_jsonapi_get_many_route(get_many)
    person_resource.add_jsonapi_get_route(get)
    person_resource.add_jsonapi_patch_route(patch)
    person_resource.add_jsonapi_post_route(post)
    person_resource.add_jsonapi_related_get_route(related_get, 'organization')
    person_resource.add_jsonapi_related_post_route(related_post, 'organization')
    person_resource.add_jsonapi_relationship_delete_route(relationship_delete, 'organization')
    person_resource.add_jsonapi_relationship_get_route(relationship_get, 'organization')
    person_resource.add_jsonapi_relationship_patch_route(relationship_patch, 'organization')
    person_resource.add_jsonapi_relationship_post_route(relationship_post, 'organization')

    person_resource.add_jsonapi_related_get_route(related_get, 'hobbies')
    person_resource.add_jsonapi_related_post_route(related_post, 'hobbies')
    person_resource.add_jsonapi_relationship_delete_route(relationship_delete, 'hobbies')
    person_resource.add_jsonapi_relationship_get_route(relationship_get, 'organization')
    person_resource.add_jsonapi_relationship_patch_route(relationship_patch, 'organization')
    person_resource.add_jsonapi_relationship_post_route(relationship_post, 'organization')
    person_resource.add_jsonapi_relationship_get_route(relationship_get, 'hobbies')

    app.include_router(person_resource)

    organization_resource.add_jsonapi_delete_route(delete)
    organization_resource.add_jsonapi_get_many_route(get_many)
    organization_resource.add_jsonapi_get_route(get)
    organization_resource.add_jsonapi_patch_route(patch)
    organization_resource.add_jsonapi_post_route(post)
    organization_resource.add_jsonapi_related_get_route(related_get, 'organization-members')
    organization_resource.add_jsonapi_related_post_route(related_post, 'organization-members')
    organization_resource.add_jsonapi_relationship_delete_route(relationship_delete, 'organization-members')
    organization_resource.add_jsonapi_relationship_get_route(relationship_get, 'organization-members')
    organization_resource.add_jsonapi_relationship_patch_route(relationship_patch, 'organization-members')
    organization_resource.add_jsonapi_relationship_post_route(relationship_post, 'organization-members')

    app.include_router(organization_resource)

    hobby_resource.add_jsonapi_delete_route(delete)
    hobby_resource.add_jsonapi_get_many_route(get_many)
    hobby_resource.add_jsonapi_get_route(get)
    hobby_resource.add_jsonapi_patch_route(patch)
    hobby_resource.add_jsonapi_post_route(post)
    hobby_resource.add_jsonapi_related_get_route(related_get, 'persons')
    hobby_resource.add_jsonapi_related_post_route(related_post, 'persons')
    hobby_resource.add_jsonapi_relationship_delete_route(relationship_delete, 'persons')
    hobby_resource.add_jsonapi_relationship_get_route(relationship_get, 'persons')
    hobby_resource.add_jsonapi_relationship_patch_route(relationship_patch, 'persons')
    hobby_resource.add_jsonapi_relationship_post_route(relationship_post, 'persons')

    app.include_router(hobby_resource)

    node_resource.add_jsonapi_get_many_route(get_many)
    node_resource.add_jsonapi_get_route(get)
    app.include_router(node_resource)

    def custom_openapi():
        if app.openapi_schema:
            return app.openapi_schema

        openapi_schema = get_openapi(
            title="FastAPI Pydantic JsonAPI",
            version="0.0.1",
            description="This is a very custom OpenAPI schema",
            routes=app.routes,
        )

        register_jsonapi_schema_components(openapi_schema)
        app.openapi_schema = openapi_schema

        return app.openapi_schema

    app.openapi = custom_openapi

    return app



