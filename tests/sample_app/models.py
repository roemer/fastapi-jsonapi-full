import pydantic as pd
from fastapi_jsonapi_full import BaseModel, Relationship

from .storage import person_hobbies


class Organization(BaseModel):
    id: int
    name: str

    class Relationships:
        organization_members = Relationship('Person', foreign_key='id', remote_side='organization_id', many=True, attribute_name='persons', reverse='organization')


class BasePerson(BaseModel):
    last_name: str = ...


class Person(BasePerson):
    id: int
    first_name: str

    class Relationships:
        organization = Relationship(Organization, foreign_key='organization_id', remote_side='id', reverse='organization-members')
        hobbies = Relationship('Hobby', foreign_key='id', remote_side='person_id', reverse='persons', secondary=person_hobbies)


class Hobby(BaseModel):
    id: int
    name: str

    class Relationships:
        persons = Relationship('Person', foreign_key='id', remote_side='hobby_id', reverse='hobbies', secondary=person_hobbies)

class Node(BaseModel):
    id: int
    name: str

    class Relationships:
        parent = Relationship('Node', foreign_key='parent_id', remote_side='id', reverse='children')

