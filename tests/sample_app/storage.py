from typing import Iterable, Optional, Any, Tuple
from copy import deepcopy
from pathlib import Path

import sqlalchemy as sa
from sqlalchemy import Table, Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, Session, Query, aliased

import logging

from fastapi_jsonapi_full.sqlalchemy import BaseDataLayer, FilterParam, SortParam

logger = logging.getLogger(__name__)

SQLITE_PATH = Path(__file__).parent.parent / 'test.sqlite'

SQLALCHEMY_DATABASE_URL = f"sqlite:///{SQLITE_PATH}?check_same_thread=False"
engine = sa.create_engine(SQLALCHEMY_DATABASE_URL)

# `expire_on_commit` is set to prevent SqlAlchemy from refreshing
# ORM objects after `session.commit` is called;
# the refresh is not needed, since we just return the data over the API
SessionLocal = sessionmaker(bind=engine, expire_on_commit=False)
Base = declarative_base()

logging.getLogger('sqlalchemy').level = logging.INFO


def get_session() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


person_hobbies = sa.Table(
    "person_hobby",
    Base.metadata,
    sa.Column("person_id", sa.Integer, sa.ForeignKey("person.id"), primary_key=True),
    sa.Column("hobby_id", sa.Integer, sa.ForeignKey("hobby.id"), primary_key=True)
)


class SamplePerson(Base):
    __tablename__ = "person"

    id: int = sa.Column(sa.Integer, primary_key=True)
    first_name: str = sa.Column(sa.String, nullable=False)
    last_name: str = sa.Column(sa.String, nullable=False)

    organization_id: Optional[int] = sa.Column(sa.Integer, sa.ForeignKey('organization.id'), nullable=True)
    organization: Optional['SampleOrganization'] = relationship('SampleOrganization', back_populates='persons')

    hobbies: list['SampleHobby'] = relationship(
        'SampleHobby',
        secondary=person_hobbies,
        back_populates='persons',
        primaryjoin="SamplePerson.id == person_hobby.c.person_id",
        secondaryjoin="SampleHobby.id == person_hobby.c.hobby_id",
    )


class SampleOrganization(Base):
    __tablename__ = "organization"
    id: int = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String, nullable=False)

    persons: list[SamplePerson] = relationship('SamplePerson', back_populates='organization')


class SampleHobby(Base):
    __tablename__ = "hobby"
    id: int = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String, nullable=False)

    persons: list['SamplePerson'] = relationship(
        'SamplePerson',
        back_populates='hobbies',
        secondary=person_hobbies,
        primaryjoin="SampleHobby.id == person_hobby.c.hobby_id",
        secondaryjoin="SamplePerson.id == person_hobby.c.person_id",
    )


class SampleNode(Base):
    __tablename__ = "node"
    id: int = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String, nullable=False)

    parent_id: int = sa.Column(sa.Integer, sa.ForeignKey('node.id'), nullable=True)
    parent = relationship('SampleNode', remote_side='SampleNode.id', back_populates='children')

    children: list['SampleNode'] = relationship(
         'SampleNode',
        back_populates='parent'
    )

organizations = [
    SampleOrganization(id=1, name='First National'),
    SampleOrganization(id=2, name='National Trust'),
    SampleOrganization(id=3, name='Ascam Ltd')
]

persons = [
    SamplePerson(id=1, first_name='John', last_name='Doe', organization_id=1),
    SamplePerson(id=2, first_name='Anna', last_name='Doe', organization_id=1),
    SamplePerson(id=3, first_name='Max', last_name='Mustermann', organization_id=2),
    SamplePerson(id=4, first_name='Erika', last_name='Mustermann', organization_id=None),
]

hobbies = [
    SampleHobby(id=1, name='soccer'),
    SampleHobby(id=2, name='stamp collecting'),
    SampleHobby(id=3, name='football'),
    SampleHobby(id=4, name='fjerljeppen'),
    SampleHobby(id=5, name='gardening'),
]

person_hobbies_values = [
    (1, 2),
    (1, 3),
    (1, 4),
    (2, 1),
    (2, 3),
]


nodes = [
    SampleNode(id=1, name="root"),
    SampleNode(id=2, name="child1", parent_id=1),
    SampleNode(id=3, name="child2", parent_id=1),
]


def create_db():
    logger.info("Creating database")
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine, checkfirst=False)

    session = SessionLocal()

    for person in persons:
        session.add(deepcopy(person))
    for organization in organizations:
        session.add(deepcopy(organization))
    for hobby in hobbies:
        session.add(deepcopy(hobby))
    for node in nodes:
        session.add(deepcopy(node))

    for person, hobby in person_hobbies_values:
        qry = sa.insert(person_hobbies).values({'person_id': person, 'hobby_id': hobby})
        session.execute(qry)

    session.commit()
    session.close()

