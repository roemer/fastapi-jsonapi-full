from typing import Iterable, Optional, Any, Tuple
from copy import deepcopy

import sqlalchemy as sa
from sqlalchemy.orm import Session, Query, aliased

import logging

from fastapi_jsonapi_full.sqlalchemy import BaseDataLayer, FilterParam, SortParam

from .storage import SamplePerson, SampleOrganization, SampleHobby, SampleNode
from .models import Person, Organization, Hobby, Node

logger = logging.getLogger(__name__)


class PersonDataLayer(BaseDataLayer):

    orm_type = SamplePerson
    model = Person

    aliased_organization = aliased(SampleOrganization)
    orm_alias_mappings = {
        None: SamplePerson,
        #'organization': aliased_organization,
        #'hobbies': SampleHobby
    }

    def base_query(
            self,
            filter: FilterParam = None,
            sort: SortParam = None,
            **kwargs
    ) -> Query:
        query = sa.select(self.orm_type) #.join(self.aliased_organization, isouter=True)
        return self.apply_filter_and_sort(query, filter, sort)


class OrganizationDataLayer(BaseDataLayer):
    orm_type = SampleOrganization
    model = Organization


class HobbyDataLayer(BaseDataLayer):
    orm_type = SampleHobby
    model = Hobby

class NodeDataLayer(BaseDataLayer):
    orm_type = SampleNode
    model = Node
