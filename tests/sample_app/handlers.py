from fastapi import Depends

from .storage import Session, get_session

import fastapi_jsonapi_full.requests as requests
import fastapi_jsonapi_full.handlers as base_handlers
from fastapi_jsonapi_full.sqlalchemy import FilterParam, SortParam, DefaultPaginationParam


async def delete(request: requests.DeleteRequest, session: Session = Depends(get_session)):
    return await base_handlers.delete(request, session=session)


async def get(request: requests.GetRequest, session: Session = Depends(get_session)):
    return await base_handlers.get(request, session=session)


async def get_many(
        request: requests.GetManyRequest,
        pagination: DefaultPaginationParam = Depends(DefaultPaginationParam),
        filter: FilterParam = Depends(FilterParam),
        sort: SortParam = Depends(SortParam),
        session: Session = Depends(get_session)
):
    return await base_handlers.get_many(request, pagination=pagination, filter=filter, sort=sort, session=session)


async def patch(request: requests.PatchRequest, session: Session = Depends(get_session)):
    return await base_handlers.patch(request, session=session)


async def post(request: requests.PostRequest, session: Session = Depends(get_session)):
    return await base_handlers.post(request, session=session)


async def related_get(
        request: requests.RelatedGetRequest,
        pagination: DefaultPaginationParam = Depends(DefaultPaginationParam),
        filter: FilterParam = Depends(FilterParam),
        sort: SortParam = Depends(SortParam),
        session: Session = Depends(get_session)
):
    return await base_handlers.related_get(
        request,
        pagination=pagination,
        filter=filter,
        sort=sort,
        session=session
    )


async def related_post(request: requests.RelatedPostRequest, session: Session = Depends(get_session)):
    return await base_handlers.related_post(request, session=session)


async def relationship_delete(request: requests.RelationshipDeleteRequest, session: Session = Depends(get_session)):
    return await base_handlers.relationship_delete(request, session=session)


async def relationship_get(
        request: requests.RelationshipGetRequest,
        pagination: DefaultPaginationParam = Depends(DefaultPaginationParam),
        filter: FilterParam = Depends(FilterParam),
        sort: SortParam = Depends(SortParam),
        session: Session = Depends(get_session)
):
    return await base_handlers.relationship_get(
        request,
        pagination=pagination,
        filter=filter,
        sort=sort,
        session=session
    )


async def relationship_patch(request: requests.RelationshipPatchRequest, session: Session = Depends(get_session)):
    return await base_handlers.relationship_patch(request, session=session)


async def relationship_post(request: requests.RelationshipPostRequest, session: Session = Depends(get_session)):
    return await base_handlers.relationship_post(request, session=session)
