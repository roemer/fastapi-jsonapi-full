from .utils import get_json_result


def test_person_delete(client):
    result = get_json_result(client.delete('/persons/1'))
    assert result == {
        'jsonapi': {'version': '1.0'},
    }


def test_hobby_delete(client):
    result = get_json_result(client.delete('/hobbies/1'))
    assert result == {
        'jsonapi': {'version': '1.0'},
    }


def test_organization_delete(client):
    result = get_json_result(client.delete('/organizations/1'))
    assert result == {
        'jsonapi': {'version': '1.0'},
    }

