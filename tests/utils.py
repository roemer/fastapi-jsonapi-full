import logging
logger = logging.getLogger(__name__)


def get_json_result(result, status_code: int = 200, content_type: str = 'application/vnd.api+json'):
    logger.debug(f"{result.status_code=} {result.headers=} {result.text=}")

    assert result.status_code == status_code, f"Unexpected status code '{result.status_code}';{result.text=}"
    assert result.headers['content-type'] == content_type, f"Unexpected content type '{result.headers['content-type']}'"
    return result.json()
