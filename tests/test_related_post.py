from fastapi_jsonapi_full import JsonAPIEnvelope

from .utils import get_json_result


def test_person_post_related(client):
    post = {
        'data': {
            'attributes': {
                'name': 'First National'
            },
            'type': 'Organization'
        }
    }
    result = get_json_result(client.post('/persons/4/organization', json=post), status_code=201)
    assert result == {
        'data': {
            'attributes': {'name': 'First National'},
            'id': '4',
            'links': {'self': '/organizations/4'},
            'relationships': {
                'organization-members': {
                    'links': {
                        'related': '/organizations/4/organization-members',
                        'self': '/organizations/4/relationships/organization-members'
                    }}},
            'type': 'Organization'},
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/4/organization',
            'related': '/persons/4/relationships/organization'
        }
    }


def test_person_post_related_with_include(client):
    post = {
        'data': {
            'attributes': {
                'name': 'First National'
            },
            'type': 'Organization'
        }
    }
    result = get_json_result(client.post('/persons/4/organization?include=organization-members', json=post), status_code=201)
    assert result == {
        'data': {
            'attributes': {'name': 'First National'},
            'id': '4',
            'links': {'self': '/organizations/4'},
            'relationships': {
                'organization-members': {
                    'data': [{'id': '4', 'type': 'Person'}],
                    'links': {
                        'related': '/organizations/4/organization-members',
                        'self': '/organizations/4/relationships/organization-members'},
                    'meta': {'count': 1}}},
            'type': 'Organization'
        },
        'included': [{
            'attributes': {'firstName': 'Erika', 'lastName': 'Mustermann'},
            'id': '4',
            'links': {'self': '/persons/4'},
            'relationships': {
                'hobbies': {'links': {
                    'related': '/persons/4/hobbies',
                    'self': '/persons/4/relationships/hobbies'
                }},
                'organization': {'links': {
                    'related': '/persons/4/organization',
                    'self': '/persons/4/relationships/organization'
                }}
            },
            'type': 'Person'}],
        'jsonapi': {'version': '1.0'},
        'links': {
            'related': '/persons/4/relationships/organization',
            'self': '/persons/4/organization'
        }
    }


def test_organization_post_related(client):
    post = {
        'data': {
            'attributes': {
                'firstName': 'John',
                'lastName': 'Doe'
            },
            'type': 'Person'
        }
    }
    result = get_json_result(client.post('/organizations/1/organization-members', json=post), status_code=201)
    assert result == {
        'data': {
            'attributes': {'firstName': 'John', 'lastName': 'Doe'},
            'id': '5',
            'links': {'self': '/persons/5'},
            'relationships': {
                'hobbies': {'links': {'related': '/persons/5/hobbies',
                                      'self': '/persons/5/relationships/hobbies'}},

                'organization': {
                    'links': {
                        'related': '/persons/5/organization',
                        'self': '/persons/5/relationships/organization'
                    }}},
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/organization-members',
            'related': '/organizations/1/relationships/organization-members'
        }
    }


def test_person_post_with_relationship_organization(client):
    post = {
        'data': {
            'attributes': {
                'firstName': 'John',
                'lastName': 'Doe'
            },
            'type': 'Person',
            'relationships': {
                "data": {
                    "type": "Organization",
                    "id": "1"
                }
            }
        }
    }
    result = get_json_result(client.post('/persons', json=post), status_code=201)
    assert result == {
        'data': {
            'type': 'Person',
            'id': '5',
            'attributes': {
                'lastName': 'Doe',
                'firstName': 'John'
            },
            'relationships': {
                'hobbies': {'links': {'related': '/persons/5/hobbies',
                                      'self': '/persons/5/relationships/hobbies'}},
                'organization': {
                    'links': {
                        'related': '/persons/5/organization',
                        'self': '/persons/5/relationships/organization'}
                }
            },
            'links':
                {'self': '/persons/5'}
        },
        'links': {'self': '/persons/5'}, 'jsonapi': {'version': '1.0'}
    }


def test_person_post_with_relationship_organization_conflicting(client):
    post = {
        'data': {
            'attributes': {
                'firstName': 'John',
                'lastName': 'Doe'
            },
            'type': 'Person',
            'relationships': {
                'organization': {
                    'data': {
                        'type': 'Organization',
                        'id': '2'
                    }
                }
            }
        }
    }
    result = get_json_result(client.post('/organizations/1/organization-members', json=post), status_code=422)
