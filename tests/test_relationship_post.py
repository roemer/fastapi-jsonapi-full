import pytest
import logging

from fastapi_jsonapi_full import JsonAPIEnvelope
import fastapi_jsonapi_full.exceptions as ex

from .utils import get_json_result

logger = logging.getLogger(__name__)


def test_person_post_relationship(client):
    post = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client.post('/persons/4/relationships/organization', json=post), status_code=201)
    assert result == {
        'data': {
            'type': 'Organization',
            'id': '2'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/4/relationships/organization',
            'related': '/persons/4/organization'
        }
    }


def test_person_post_existing_organization_relationship(client):
    post = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client.post('/persons/1/relationships/organization', json=post), status_code=403)


def test_person_post_relationship_with_include(client):
    post = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client.post('/persons/4/relationships/organization?include=organization', json=post), status_code=201)
    assert result == {
        'data': {
            'type': 'Organization',
            'id': '2'
        },
        'included': [{'attributes': {'name': 'National Trust'},
                      'id': '2',
                      'links': {'self': '/organizations/2'},
                      'relationships': {'organization-members': {'links': {'related': '/organizations/2/organization-members',
                                                              'self': '/organizations/2/relationships/organization-members'}}},
                      'type': 'Organization'}],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/4/relationships/organization',
            'related': '/persons/4/organization',
        }
    }


def test_person_post_relationship_many(client):
    post = {
        'data': [{
                'type': 'Organization',
                'id': '1'
            },
            {
                'type': 'Organization',
                'id': '2'
            }
        ]
    }
    result = get_json_result(client.post('/persons/1/relationships/organization', json=post), status_code=422)


def test_organization_post_relationship_one(client):
    post = {
        'data': {
            'type': 'Person',
            'id': '4'
        }
    }
    result = get_json_result(client.post('/organizations/1/relationships/organization-members', json=post), status_code=201)
    assert result == {
        'data': {
            'type': 'Person',
            'id': '4'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/relationships/organization-members',
            'related': '/organizations/1/organization-members'
        }
    }


def test_organization_post_relationship_all_one_by_one(client):
    for i in range(1, 5):
        logger.info(f"test_organization_post_relationship_all_one_by_one: {i}")
        patch = {
            'data': {
                'type': 'Person',
                'id': f'{i}'
            }
        }
        result = get_json_result(client.post('/organizations/1/relationships/organization-members', json=patch), status_code=201)
        assert result == {
            'data': {
                'type': 'Person',
                'id': f'{i}'
            },
            'jsonapi': {'version': '1.0'},
            'links': {
                'self': '/organizations/1/relationships/organization-members',
                'related': '/organizations/1/organization-members'
            }
        }


def test_organization_post_relationship_none(client):
    post = {
        'data': None
    }
    result = get_json_result(client.post('/organizations/1/relationships/organization-members', json=post), status_code=422)


def test_organization_post_relationship_empty(client):
    post = {
        'data': []
    }
    result = get_json_result(client.post('/organizations/1/relationships/organization-members', json=post), status_code=422)


def test_organization_post_relationship_many(client):
    post = {
        'data': [{
            'type': 'Person',
            'id': '1'
            }, {
                'type': 'Person',
                'id': '3'
            }]
    }
    result = get_json_result(client.post('/organizations/1/relationships/organization-members', json=post), status_code=422)


