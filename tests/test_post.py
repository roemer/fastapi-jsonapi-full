from fastapi_jsonapi_full import JsonAPIEnvelope

from .utils import get_json_result


def test_person_post(client):
    post = {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'type': 'Person'
        }
    }
    # envelope = JsonAPIEnvelope.model_validate(post).model_dump()

    result = get_json_result(client.post('/persons', json=post), status_code=201)
    assert result == {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'id': '5',
            'links': {'self': '/persons/5'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/5/hobbies',
                        'self': '/persons/5/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/5/organization',
                        'self': '/persons/5/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/5'}
    }


def test_person_post_relationship(client):
    post = {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'type': 'Person',
            'relationships': {
                'organization': {
                    'data': {
                        'type': 'Organization',
                        'id': '2'
                    }
                }
            },
        }
    }
    envelope = JsonAPIEnvelope.model_validate(post).model_dump(exclude_unset=True, exclude_none=True)

    result = get_json_result(client.post('/persons?include=organization', json=envelope), status_code=201)
    assert result == {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'id': '5',
            'links': {'self': '/persons/5'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/5/hobbies',
                        'self': '/persons/5/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/5/organization',
                        'self': '/persons/5/relationships/organization'
                    },
                    'data': {
                        'type': 'Organization',
                        'id': '2'
                    }
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/5'},
        'included': [
            {
                'attributes': {'name': 'National Trust'},
                'id': '2',
                'links': {'self': '/organizations/2'},
                'relationships': {
                    'organization-members': {
                        'links': {
                            'related': '/organizations/2/organization-members',
                            'self': '/organizations/2/relationships/organization-members'}}},
                'type': 'Organization'}],
    }


def test_person_post_empty_relationship(client):
    post = {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'type': 'Person',
            'relationships': {
                'organization': {
                }
            },
        }
    }
    envelope = JsonAPIEnvelope.model_validate(post).model_dump(exclude_unset=True, exclude_none=True)

    result = get_json_result(client.post('/persons?include=organization', json=envelope), status_code=201)
    assert result == {
        'data': {
            'attributes': {'firstName': 'Jan', 'lastName': 'De Vries'},
            'id': '5',
            'links': {'self': '/persons/5'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/5/hobbies',
                        'self': '/persons/5/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/5/organization',
                        'self': '/persons/5/relationships/organization'
                    },
                    'data': None
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/5'},
    }

