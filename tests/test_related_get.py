from .sample_app.models import Person
from .utils import get_json_result


def test_person_get_organization_relationship(client):
    result = get_json_result(client.get('/persons/1/organization'))
    assert result == {
        'data': {
             'attributes': {'name': 'First National'},
             'id': '1',
             'links': {
                 'self': '/organizations/1'
             },
             'relationships': {
                 'organization-members': {
                     'links': {
                         'related': '/organizations/1/organization-members',
                         'self': '/organizations/1/relationships/organization-members'}
                 }
             },
             'type': 'Organization'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'related': '/persons/1/relationships/organization',
            'self': '/persons/1/organization'
        }
    }


def test_person_get_organization_relationship_with_organization(client):
    result = get_json_result(client.get('/persons/1/organization?include=organization'), status_code=422)


def test_person_get_organization_relationship_empty(client):
    result = get_json_result(client.get('/persons/4/organization'))
    assert result == {
        'data': None,
        'jsonapi': {'version': '1.0'},
        'links': {
            'related': '/persons/4/relationships/organization',
            'self': '/persons/4/organization'
        }
    }


def test_person_get_organization_relationship_include_organization_organization_members(client):
    result = get_json_result(client.get('/persons/1/organization?include=organization-members'))
    included = result['included']
    included_refs = [{'id': inc['id'], 'type': inc['type']} for inc in included]
    assert included_refs == [
        {'id': '1', 'type': 'Person'},
        {'id': '2', 'type': 'Person'},
    ]


def test_organization_get_persons_relationship(client):
    result = get_json_result(client.get('/organizations/1/organization-members'))
    assert result == {
        'data': [
            {
                'attributes': {'firstName': 'John', 'lastName': 'Doe'},
                'id': '1',
                'links': {'self': '/persons/1'},
                'relationships': {
                    'hobbies': {
                        'links': {
                            'related': '/persons/1/hobbies',
                            'self': '/persons/1/relationships/hobbies'
                        }
                    },
                    'organization': {'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'}}},
                'type': 'Person'
            },
            {
                'attributes': {'firstName': 'Anna', 'lastName': 'Doe'},
                'id': '2',
                'links': {'self': '/persons/2'},
                'relationships': {
                    'hobbies': {
                        'links': {
                            'related': '/persons/2/hobbies',
                            'self': '/persons/2/relationships/hobbies'
                        }
                    },
                    'organization': {'links': {'related': '/persons/2/organization', 'self': '/persons/2/relationships/organization'}}},
                'type': 'Person'
            }
        ],
        'jsonapi': {'version': '1.0'},
        'links': {
            'first': '/organizations/1/organization-members?page%5Boffset%5D=0',
            'last': '/organizations/1/organization-members?page%5Boffset%5D=0',
            'related': '/organizations/1/relationships/organization-members',
            'self': '/organizations/1/organization-members'
        },
        'meta': {'count': 2}
    }


def test_organization_get_persons_relationship_single(client):
    result = get_json_result(client.get('/organizations/2/organization-members'))
    assert result == {
        'data': [
            {
                'attributes': {'firstName': 'Max', 'lastName': 'Mustermann'},
                'id': '3',
                'links': {'self': '/persons/3'},
                'relationships': {
                    'hobbies': {
                        'links': {
                            'related': '/persons/3/hobbies',
                            'self': '/persons/3/relationships/hobbies'
                        }
                    },
                    'organization': {'links': {'related': '/persons/3/organization', 'self': '/persons/3/relationships/organization'}}
                },
                'type': 'Person'
            }
        ],
        'jsonapi': {'version': '1.0'},
        'links': {
            'first': '/organizations/2/organization-members?page%5Boffset%5D=0',
            'last': '/organizations/2/organization-members?page%5Boffset%5D=0',
            'related': '/organizations/2/relationships/organization-members',
            'self': '/organizations/2/organization-members'
        },
        'meta': {'count': 1}
    }


def test_organization_get_persons_relationship_empty(client):
    result = get_json_result(client.get('/organizations/3/organization-members'))
    assert result == {
        'data': [],
        'jsonapi': {'version': '1.0'},
        'links': {
            'related': '/organizations/3/relationships/organization-members',
            'self': '/organizations/3/organization-members'
        },
        'meta': {'count': 0}
    }


def test_organization_get_persons_relationship_paginated_limit_1(client):
    result = get_json_result(client.get('/organizations/1/organization-members?page[limit]=1'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['links'] == {
        'first': '/organizations/1/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/organizations/1/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'next': '/organizations/1/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'related': '/organizations/1/relationships/organization-members',
        'self': '/organizations/1/organization-members'
    }
    assert result['meta'] == {
        'count': 2
    }


