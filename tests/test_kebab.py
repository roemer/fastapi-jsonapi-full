import pytest
from fastapi_jsonapi_full.convert_case import to_kebab_case, to_camel_case, to_pascal_case, to_snake_case


@pytest.mark.parametrize('name, expected', (
        ('PascalIn', 'pascal-in'),
        ('PascalIN', 'pascal-in'),
        ('camelIn', 'camel-in'),
        ('camelIN', 'camel-in'),
        ('snake_in', 'snake-in'),
        ('SNAKE_IN', 'snake-in'),
        ('SnAkE_In', 'snake-in'),
        ('', ''),
))
def test_to_kebab(name: str, expected: str):
    assert to_kebab_case(name) == expected


@pytest.mark.parametrize('name, expected', (
        ('PascalIn', 'pascal_in'),
        ('PascalIN', 'pascal_in'),
        ('camelIn', 'camel_in'),
        ('camelIN', 'camel_in'),
        ('snake_in', 'snake_in'),
        ('SNAKE_IN', 'snake_in'),
        ('SnAkE_In', 'snake_in'),
        ('', ''),
))
def test_to_snake(name: str, expected: str):
    assert to_snake_case(name) == expected


@pytest.mark.parametrize('name, expected', (
        ('PascalIn', 'PascalIn'),
        ('PascalIN', 'PascalIn'),
        ('camelIn', 'CamelIn'),
        ('camelIN', 'CamelIn'),
        ('snake_in', 'SnakeIn'),
        ('SNAKE_IN', 'SnakeIn'),
        ('SNAKE_IN', 'SnakeIn'),
        ('SnAkE_In', 'SnakeIn'),
        ('', ''),
))
def test_to_pascal(name: str, expected: str):
    assert to_pascal_case(name) == expected


@pytest.mark.parametrize('name, expected', (
        ('PascalIn', 'pascalIn'),
        ('PascalIN', 'pascalIn'),
        ('camelIn', 'camelIn'),
        ('camelIN', 'camelIn'),
        ('snake_in', 'snakeIn'),
        ('SNAKE_IN', 'snakeIn'),
        ('SNAKE_IN', 'snakeIn'),
        ('SnAkE_In', 'snakeIn'),
        ('', ''),
))
def test_to_camel(name: str, expected: str):
    assert to_camel_case(name) == expected
