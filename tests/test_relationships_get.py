from .sample_app.models import Person
from .utils import get_json_result


def test_person_get_organization_relationship(client):
    result = get_json_result(client.get('/persons/1/relationships/organization'))
    assert result == {
        'data': {
            'type': 'Organization',
            'id': '1'
        },
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_person_get_organization_relationship_empty(client):
    result = get_json_result(client.get('/persons/4/relationships/organization'))
    assert result == {
        'data': None,
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/4/relationships/organization',
            'related': '/persons/4/organization'
        }
    }


def test_person_get_organization_relationship_include_organization_organization_members(client):
    result = get_json_result(client.get('/persons/1/relationships/organization?include=organization.organization-members'))
    included = result['included']
    included_refs = [{'id': inc['id'], 'type': inc['type']} for inc in included]
    assert included_refs == [
        {'id': '1', 'type': 'Organization'},
        {'id': '1', 'type': 'Person'},
        {'id': '2', 'type': 'Person'},
    ]

    assert result == {
        'data': {'type': 'Organization', 'id': '1'},
        'included': [
            {
                'type': 'Organization',
                'id': '1',
                'attributes': {'name': 'First National'},
                'relationships': {
                    'organization-members': {
                        'links': {
                            'related': '/organizations/1/organization-members',
                            'self': '/organizations/1/relationships/organization-members'
                        },
                        'data': [{'type': 'Person', 'id': '1'}, {'type': 'Person', 'id': '2'}],
                        'meta': {'count': 2}
                    }
                },
                'links': {'self': '/organizations/1'}
            },
            {
                'type': 'Person',
                'id': '1',
                'attributes': {'firstName': 'John', 'lastName': 'Doe'},
                'relationships': {
                    'hobbies': {
                        'links': {'related': '/persons/1/hobbies', 'self': '/persons/1/relationships/hobbies'}},
                    'organization': {
                        'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'}}
                },
                'links': {'self': '/persons/1'}
            },
            {
                'type': 'Person',
                'id': '2',
                'attributes': {'firstName': 'Anna', 'lastName': 'Doe'},
                'relationships': {
                    'hobbies': {
                        'links': {'related': '/persons/2/hobbies', 'self': '/persons/2/relationships/hobbies'}},
                    'organization': {
                        'links': {'related': '/persons/2/organization', 'self': '/persons/2/relationships/organization'}}
                },
                'links': {'self': '/persons/2'}}
        ],
        'links': {'self': '/persons/1/relationships/organization', 'related': '/persons/1/organization'},
        'jsonapi': {'version': '1.0'}
    }


def test_person_get_organization_relationship_include_organization(client):
    result = get_json_result(client.get('/persons/1/relationships/organization?include=organization'))
    included = result['included']
    included_refs = [{'id': inc['id'], 'type': inc['type']} for inc in included]
    assert included_refs == [
        {'id': '1', 'type': 'Organization'}
    ]


def test_organization_get_persons_relationship(client):
    result = get_json_result(client.get('/organizations/1/relationships/organization-members'))
    assert result == {
        'data': [
            {
                'type': 'Person',
                'id': '1'
            },
            {
                'type': 'Person',
                'id': '2'
            }
        ],
        'jsonapi': {'version': '1.0'},
        'links': {
            'first': '/organizations/1/relationships/organization-members?page%5Boffset%5D=0',
            'last': '/organizations/1/relationships/organization-members?page%5Boffset%5D=0',
            'related': '/organizations/1/organization-members',
            'self': '/organizations/1/relationships/organization-members'
        },
        'meta': {'count': 2}
    }


def test_organization_get_persons_relationship_single(client):
    result = get_json_result(client.get('/organizations/2/relationships/organization-members'))
    assert result == {
        'data': [
            {
                'type': 'Person',
                'id': '3'
            }
        ],
        'jsonapi': {'version': '1.0'},
         'links': {
            'first': '/organizations/2/relationships/organization-members?page%5Boffset%5D=0',
            'last': '/organizations/2/relationships/organization-members?page%5Boffset%5D=0',
            'related': '/organizations/2/organization-members',
            'self': '/organizations/2/relationships/organization-members'
         },
        'meta': {'count': 1}
    }


def test_organization_get_persons_relationship_empty(client):
    result = get_json_result(client.get('/organizations/3/relationships/organization-members'))
    assert result == {
        'data': [],
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/3/relationships/organization-members',
            'related': '/organizations/3/organization-members'
        },
        'meta': {'count': 0}
    }


def test_organization_get_persons_relationship_paginated_limit_1(client):
    result = get_json_result(client.get('/organizations/1/relationships/organization-members?page[limit]=1'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['links'] == {
        'first': '/organizations/1/relationships/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/organizations/1/relationships/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'next': '/organizations/1/relationships/organization-members?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'related': '/organizations/1/organization-members',
        'self': '/organizations/1/relationships/organization-members'
    }
    assert result['meta'] == {
        'count': 2
    }
