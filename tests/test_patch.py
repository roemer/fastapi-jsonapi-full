import pytest
from fastapi_jsonapi_full import JsonAPIEnvelope

from .utils import get_json_result


def test_person_patch(client):
    patch = {
        'data': {
            'attributes': {'firstName': 'Johnny'},
            'id': '1',
            'type': 'Person'
        }
    }
    result = get_json_result(client.patch('/persons/1', json=patch))
    assert result == {
        'data': {
            'attributes': {'firstName': 'Johnny', 'lastName': 'Doe'},
            'id': '1',
            'links': {'self': '/persons/1'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/1/hobbies',
                        'self': '/persons/1/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/1/organization',
                        'self': '/persons/1/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/1'}
    }


def test_person_patch_relationship_none(client):
    patch = {
        'data': {
            'attributes': {'firstName': 'Johnny'},
            'id': '1',
            'type': 'Person',
            'relationships': {
                'organization': {
                    'data': {
                        'type': 'Organization',
                        'id': '2'
                    }
                }
            },
        }
    }
    result = get_json_result(client.patch('/persons/1?include=organization', json=patch))
    assert result == {
        'data': {
            'attributes': {'firstName': 'Johnny', 'lastName': 'Doe'},
            'id': '1',
            'links': {'self': '/persons/1'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/1/hobbies',
                        'self': '/persons/1/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/1/organization',
                        'self': '/persons/1/relationships/organization'
                    },
                    'data': {
                        'type': 'Organization',
                        'id': '2'
                    }
                }
            },
            'type': 'Person'
        },
        'included': [{'attributes': {'name': 'National Trust'},
                      'id': '2',
                      'links': {'self': '/organizations/2'},
                      'relationships': {'organization-members': {'links': {'related': '/organizations/2/organization-members',
                                                              'self': '/organizations/2/relationships/organization-members'}}},
                      'type': 'Organization'}],
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/1'}
    }


# @pytest.mark.xfail
def test_person_patch_relationship_empty(client):
    patch = {
        'data': {
            'attributes': {'firstName': 'Johnny'},
            'id': '1',
            'type': 'Person',
            'relationships': {
                'organization': {
                    'data': []
                }
            },
        }
    }
    result = get_json_result(client.patch('/persons/1?include=organization', json=patch), status_code=422)

    assert result == {
        'errors': [{
            'code': 'UnexpectedContent',
            'detail': "Expected object or None, but found list for relation 'Person.organization'",
            'status': '422',
            'title': 'Unexpected content'
        }],
        'jsonapi': {'version': '1.0'}
    }
