from .utils import get_json_result


def test_person_get_invalid_id(client):
    result = get_json_result(client.get('/persons/1234'), status_code=404)
    assert result == {
        'errors': [{'code': 'ItemNotFound',
                    'detail': "Item with id '1234' not found",
                    'status': '404',
                    'title': 'Item not found'}],
        'jsonapi': {'version': '1.0'}
    }


def test_person_get_invalid_id_format(client):
    result = get_json_result(client.get('/persons/abcd'), status_code=422)


def test_person_get_invalid_relationship(client):
    result = get_json_result(client.get('/persons/1?include=invalid'), status_code=422)
    assert result == {
        'errors': [{'code': 'RelationshipNotFound',
                    'detail': "Unknown relationship 'Person.invalid'",
                    'status': '422',
                    'title': 'Relationship not found'}],
        'jsonapi': {'version': '1.0'}}


def test_person_get_invalid_deep_relationship(client):
    result = get_json_result(client.get('/persons/1?include=organization.invalid'), status_code=422)
    assert result == {
        'errors': [{'code': 'RelationshipNotFound',
                    'detail': "Unknown relationship 'Organization.invalid'",
                    'status': '422',
                    'title': 'Relationship not found'}],
        'jsonapi': {'version': '1.0'}}


def test_person_post_wrong_body(client):
    post = {
        'data': {
            'attributes': {'firstName': 'Jan', 'wrongAttribute': 'foo'},
            'type': 'Person'
        }
    }
    result = get_json_result(client.post('/persons', json=post), status_code=422)
    assert result == {
        'errors': [{'code': 'UnprocessableContent',
                    'detail': '1 validation error for Person__POST\n'
                              'lastName\n'
                              '  Field required [type=missing, '
                              "input_value={'firstName': 'Jan', 'wrongAttribute': "
                              "'foo'}, input_type=dict]\n"
                              '    For further information visit '
                              'https://errors.pydantic.dev/2.10/v/missing',
                    'status': '422',
                    'title': 'Unprocessable content'}],
        'jsonapi': {'version': '1.0'}
    }
