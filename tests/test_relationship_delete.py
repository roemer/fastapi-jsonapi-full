from typing import Any
import pytest
from fastapi.testclient import TestClient

from fastapi_jsonapi_full import JsonAPIEnvelope
import fastapi_jsonapi_full.exceptions as ex

from .utils import get_json_result


def client_delete(client: TestClient, url: str, json: Any = None):
    return client.request("DELETE", url, json=json)


def test_person_delete_relationship_simple(client):
    delete = {
        'data': {
            'type': 'Organization',
            'id': '2'
        }
    }
    result = get_json_result(client_delete(client, '/persons/1/relationships/organization', json=delete))
    assert result == {
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_person_delete_relationship_list(client):
    delete = {
        'data': [{
            'type': 'Organization',
            'id': '2'
        }]
    }
    result = get_json_result(client_delete(client, '/persons/1/relationships/organization', json=delete))
    assert result == {
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/persons/1/relationships/organization',
            'related': '/persons/1/organization'
        }
    }


def test_person_delete_relationship_none(client):
    delete = {
        'data': None
    }
    result = get_json_result(client_delete(client, '/persons/1/relationships/organization', json=delete), status_code=422)


def test_person_delete_relationship_empty(client):
    delete = {
        'data': []
    }
    result = get_json_result(client_delete(client, '/persons/1/relationships/organization', json=delete), status_code=422)


def test_person_delete_relationship_many(client):
    delete = {
        'data': [{
                'type': 'Organization',
                'id': '1'
            },
            {
                'type': 'Organization',
                'id': '2'
            }
        ]
    }
    result = get_json_result(client_delete(client, '/persons/1/relationships/organization', json=delete), status_code=422)


def test_organization_delete_relationship_single(client):
    delete = {
        'data': {
            'type': 'Person',
            'id': '2'
        }
    }
    result = get_json_result(client_delete(client, '/organizations/1/relationships/organization-members', json=delete))


@pytest.mark.xfail
def test_organization_delete_relationship_not_found(client):
    delete = {
        'data': {
            'type': 'Person',
            'id': '3'
        }
    }
    result = get_json_result(client_delete(client, '/organizations/1/relationships/organization-members', json=delete))


def test_organization_delete_relationship_many(client):
    delete = {
        'data': [{
            'type': 'Person',
            'id': '2'
        }]
    }
    result = get_json_result(client_delete(client, '/organizations/1/relationships/organization-members', json=delete))
    assert result == {
        'jsonapi': {'version': '1.0'},
        'links': {
            'self': '/organizations/1/relationships/organization-members',
            'related': '/organizations/1/organization-members'
        }
    }


def test_organization_delete_relationship_empty(client):
    delete = {
        'data': []
    }
    result = get_json_result(client_delete(client, '/organizations/1/relationships/organization-members', json=delete), status_code=422)


def test_organization_delete_relationship_none(client):
    delete = {
        'data': None
    }
    result = get_json_result(client_delete(client, '/organizations/1/relationships/organization-members', json=delete), status_code=422)


