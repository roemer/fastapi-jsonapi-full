import pytest
import pydantic

from .sample_app.models import Person
from fastapi_jsonapi_full.schema import JsonAPIRecord

# def test_person_schema():
#     schema, definitions = Person.__jsonapi_openapi_schema__, Person.__jsonapi_openapi_definitions__
#     assert schema == {
#         'properties': {
#             'attributes': {'$ref': '#/components/schemas/PersonAttributes'},
#             'id': {'title': 'Id', 'type': 'string'},
#             'relationships': {'$ref': '#/components/schemas/JsonAPIRelationships'},
#             'type': {'enum': ['Person'], 'title': 'Type', 'type': 'string'},
#             'links': {'$ref': '#/components/schemas/JsonAPILinks'},
#         },
#         'required': ['type'],
#         'title': 'Person',
#         'type': 'object'
#     }
#
#     assert definitions == {
#         'JsonAPILinks': {'additionalProperties': {'type': 'string'},
#                          'title': 'JsonAPILinks',
#                          'type': 'object'},
#         'JsonAPIRecordLink': {'properties': {'id': {'title': 'Id', 'type': 'string'},
#                                             'type': {'title': 'Type',
#                                                      'type': 'string'}},
#                              'required': ['type', 'id'],
#                              'title': 'JsonAPIRecordLink',
#                              'type': 'object'},
#         'JsonAPIRelationship': {
#             'properties': {'data': {'anyOf': [{'$ref': '#/components/schemas/JsonAPIRecordLink'},
#                                               {'items': {
#                                                   '$ref': '#/components/schemas/JsonAPIRecordLink'},
#                                                   'type': 'array'}],
#                                     'title': 'Data'},
#                            'links': {'$ref': '#/components/schemas/JsonAPILinks'},
#                            'meta': {'title': 'Meta',
#                                     'type': 'object'},
#                            },
#             'title': 'JsonAPIRelationship',
#             'type': 'object'},
#         'JsonAPIRelationships': {
#             'additionalProperties': {'$ref': '#/components/schemas/JsonAPIRelationship'},
#             'title': 'JsonAPIRelationships',
#             'type': 'object'},
#         'PersonAttributes': {'properties': {'firstName': {'title': 'Firstname',
#                                                           'type': 'string'},
#                                             'lastName': {'title': 'Lastname',
#                                                          'type': 'string'}},
#                              'title': 'PersonAttributes',
#                              'type': 'object'}}


def test_default_schema():
    person = Person(**{'id': 1, 'firstName': 'John', 'lastName': 'Doe'})
    assert person.model_dump() == {'first_name': 'John', 'id': 1, 'last_name': 'Doe', 'meta': None}


def test_default_schema_missing_id():
    with pytest.raises(pydantic.ValidationError):
        Person(**{'firstName': 'John', 'lastName': 'Doe'})


def test_default_schema_missing_last_name():
    with pytest.raises(pydantic.ValidationError):
        Person(**{'id': 1, 'firstName': 'John'})


def test_post_schema():
    person = Person._jsonapi_post_model(**{'id': 1, 'firstName': 'John', 'lastName': 'Doe'})
    assert person.model_dump() == {'first_name': 'John', 'id': 1, 'last_name': 'Doe'}


def test_post_schema_missing_id():
    person = Person._jsonapi_post_model(**{'firstName': 'John', 'lastName': 'Doe'})
    assert person.model_dump() == {'first_name': 'John', 'id': None, 'last_name': 'Doe'}


def test_post_schema_missing_last_name():
    with pytest.raises(pydantic.ValidationError):
        Person._jsonapi_post_model(**{'id': 1, 'firstName': 'John'})


def test_patch_schema():
    person = Person._jsonapi_patch_model(**{'id': 1, 'firstName': 'John', 'lastName': 'Doe'})
    assert person.model_dump() == {'first_name': 'John', 'id': 1, 'last_name': 'Doe'}


def test_patch_schema_missing_id():
    with pytest.raises(pydantic.ValidationError):
        Person._jsonapi_patch_model(**{'firstName': 'John', 'lastName': 'Doe'})


def test_patch_schema_missing_last_name():
    person = Person._jsonapi_patch_model(**{'id': 1, 'firstName': 'John'})
    assert person.model_dump() == {'first_name': 'John', 'id': 1, 'last_name': None}


def test_jsonapi_serialization():
    person = Person(**{'id': 1, 'firstName': 'John', 'lastName': 'Doe'})
    Person._generate_jsonapi_record_model()
    js = person.jsonapi_to_record('/persons').model_dump()
    assert js == {
        'attributes': {'first_name': 'John', 'last_name': 'Doe'},
        'id': '1',
        'links': {'self': '/persons/1'},
        'meta': None,
        'relationships': {
            'hobbies': {
                'data': None,
                'links': {'related': '/persons/1/hobbies', 'self': '/persons/1/relationships/hobbies'},
                'meta': None
            },
            'organization': {
               'data': None,
               'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'},
               'meta': None
            }},
        'type': 'Person'
    }


def test_jsonapi_serialization_with_meta():
    person = Person(**{
        'id': 1,
        'firstName': 'John',
        'lastName': 'Doe',
        'meta': {
            'permissions': {
                'update': True
            },
            '_relationships': {
                'organization': {
                    'permissions': {
                        'create': True
                    }
                }
            }
        }
    })
    Person._generate_jsonapi_record_model()
    js = person.jsonapi_to_record('/persons').model_dump()
    assert js == {
        'attributes': {'first_name': 'John', 'last_name': 'Doe'},
        'id': '1',
        'links': {'self': '/persons/1'},
        'meta': {
            'permissions': {
                'update': True
            }
        },
        'relationships': {
            'hobbies': {
                'data': None,
                'links': {
                    'related': '/persons/1/hobbies',
                    'self': '/persons/1/relationships/hobbies'
                },
                'meta': None
            },
            'organization': {
                'data': None,
                'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'},
                'meta': {
                    'permissions': {
                        'create': True
                    }
                }
            }},
        'type': 'Person'
    }


def test_jsonapi_deserialization_with_meta():
    Person._generate_jsonapi_record_model()
    record = JsonAPIRecord.model_validate({
        'attributes': {'first_name': 'John', 'last_name': 'Doe'},
        'id': '1',
        'links': {'self': '/persons/1'},
        'meta': {'update': True},
        'relationships': {
            'organization': {
               'data': None,
               'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'},
               'meta': None
            }},
        'type': 'Person'
    })
    person = Person.jsonapi_from_record(record)
    assert person.model_dump() == {'first_name': 'John', 'id': 1, 'last_name': 'Doe', 'meta': None}
