import pytest
import logging
from fastapi.testclient import TestClient

logger = logging.getLogger(__name__)
logging.basicConfig()

from .sample_app.app import create_app, create_db


@pytest.fixture(scope='function')
def client():
    logger.info('Creating test client')
    create_db()
    app = create_app()
    with TestClient(app) as client:
        yield client

