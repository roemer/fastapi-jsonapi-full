import pytest
import json

from .utils import get_json_result


def test_person_list_all(client):
    result = get_json_result(client.get('/persons'))
    data = result['data']
    assert len(data) == 4


def test_organization_list_all(client):
    result = get_json_result(client.get('/organizations'))
    data = result['data']
    assert len(data) == 3


def test_person_list_paginated_limit_1(client):
    result = get_json_result(client.get('/persons?page[limit]=1'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=3',
        'next': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    result = get_json_result(client.get(result['links']['next']))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '2'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=3',
        'next': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=2',
        'previous': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    result = get_json_result(client.get(result['links']['next']))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '3'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=3',
        'next': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=3',
        'previous': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=1',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    result = get_json_result(client.get(result['links']['next']))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '4'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=3',
        'previous': '/persons?page%5Blimit%5D=1&page%5Boffset%5D=2',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    # go beyond result set, should return empty result
    result = get_json_result(client.get('/persons?page[limit]=1&page[offset]=4'))
    data = result['data']
    assert len(data) == 0
    assert result['meta'] == {
        'count': 4
    }


def test_person_list_paginated_limit_2(client):
    result = get_json_result(client.get('/persons?page[limit]=2'))
    data = result['data']
    assert len(data) == 2
    assert data[0]['id'] == '1'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=2',
        'next': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=2',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    result = get_json_result(client.get(result['links']['next']))
    data = result['data']
    assert len(data) == 2
    assert data[0]['id'] == '3'
    assert result['links'] == {
        'first': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=0',
        'last': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=2',
        'previous': '/persons?page%5Blimit%5D=2&page%5Boffset%5D=0',
        'self': '/persons'
    }
    assert result['meta'] == {
        'count': 4
    }

    # go beyond result set, should return empty result
    result = get_json_result(client.get('/persons?page[limit]=1&page[offset]=4'))
    data = result['data']
    assert len(data) == 0
    assert result['meta'] == {
        'count': 4
    }

def test_person_list_paginated_with_multiple_includes(client):
    # call without offset
    # this should return the correct organization
    result = get_json_result(client.get('/persons?page[limit]=1&include=organization,hobbies'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['meta'] == {
        'count': 4
    }
    assert data[0]['relationships']['organization']['data']['id'] == '1'

def test_person_list_paginated_with_includes_limit_1(client):
    # call without offset
    # this should return the correct organization
    result = get_json_result(client.get('/persons?page[limit]=1&include=organization'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['meta'] == {
        'count': 4
    }
    assert data[0]['relationships']['organization']['data']['id'] == '1'

    # call with offset;
    # this should still return the same organization, meaning the offset is not applied to the include
    result = get_json_result(client.get('/persons?page[limit]=1&page[offset]=1&include=organization'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '2'
    assert result['meta'] == {
        'count': 4
    }
    assert data[0]['relationships']['organization']['data']['id'] == '1'


def test_person_list_paginated_with_includes_limit_2(client):
    # Call with limit. This should not apply to includes
    result = get_json_result(client.get('/persons?page[limit]=1&include=hobbies'))
    data = result['data']
    assert len(data) == 1
    assert data[0]['id'] == '1'
    assert result['meta'] == {
        'count': 4
    }
    assert result['included'][0]['id'] == '2'
    assert len(result['included']) == 3


def test_person_list_filter_brackets(client):
    result = get_json_result(client.get('/persons?filter[firstName]=Max'))
    data = result['data']
    assert len(data) == 1


def test_person_list_filter(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "firstName", "val": "Max"}]'))
    data = result['data']
    assert len(data) == 1


def test_node_filter_parent(client):
    result = get_json_result(client.get('/nodes?filter=[{"name": "parent.name", "val": "root"}]'))
    data = result['data']
    assert len(data) == 2

def test_person_list_filter_multiple(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "firstName", "val": "Max"},{"name": "lastName", "val": "Mustermann"}]'))
    data = result['data']
    assert len(data) == 1


def test_person_list_sort_id_descending(client):
    result = get_json_result(client.get('/persons?sort=-id'))
    data = result['data']
    assert len(data) == 4
    ids = [row['id'] for row in data]
    assert ids == ['4', '3', '2', '1']


def test_person_list_sort_name_ascending(client):
    result = get_json_result(client.get('/persons?sort=lastName,-firstName'))
    data = result['data']
    assert len(data) == 4
    ids = [row['id'] for row in data]
    assert ids == ['1', '2', '3', '4']


def test_organization_list_sort_organization_name(client):
    result = get_json_result(client.get('/organizations?sort=name'))
    data = result['data']
    assert len(data) == 3
    ids = [row['id'] for row in data]
    assert ids == ['3', '1', '2']


def test_person_list_sort_organization_name_lastname(client):
    result = get_json_result(client.get('/persons?sort=organization.name,lastName'))
    data = result['data']
    assert len(data) == 4
    ids = [row['id'] for row in data]
    assert ids == ['4', '1', '2', '3']


def test_person_list_related_filter(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "organization.name", "val": "National Trust"}]'))
    data = result['data']
    assert len(data) == 1


def test_person_list_related_filter_like(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "organization.name", "val": "%National%", "op": "like"}]'))
    data = result['data']
    assert len(data) == 3


@pytest.mark.xfail
def test_person_list_related_filter_like2(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "organization.name", "val": "%national%", "op": "like"}]'))
    data = result['data']
    assert len(data) == 0


def test_person_list_related_filter_ilike(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "organization.name", "val": "%national%", "op": "ilike"}]'))
    data = result['data']
    assert len(data) == 3


def test_person_list_filter_startswith(client):
    result = get_json_result(client.get('/persons?filter=[{"name": "lastName", "val": "Mus", "op": "startswith"}]'))
    data = result['data']
    assert len(data) == 2


def test_person_list_filter_eq(client):
    result = get_json_result(client.get('/persons?filter={"name": "firstName", "val": "Anna", "op": "eq"}'))
    data = result['data']
    assert len(data) == 1

def test_person_list_filter_ieq(client):
    result = get_json_result(client.get('/persons?filter={"name": "firstName", "val": "anna", "op": "ieq"}'))
    data = result['data']
    assert len(data) == 1


def test_person_list_filter_eq_implicit(client):
    result = get_json_result(client.get('/persons?filter={"name": "firstName", "val": "Anna"}'))
    data = result['data']
    assert len(data) == 1


def test_person_list_filter_ne(client):
    result = get_json_result(client.get('/persons?filter={"name": "firstName", "val": "Anna", "op": "ne"}'))
    data = result['data']
    assert len(data) == 3


def test_person_list_filter_gt(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": "2", "op": "gt"}'))
    data = result['data']
    assert len(data) == 2


def test_person_list_filter_ge(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": "2", "op": "ge"}'))
    data = result['data']
    assert len(data) == 3


def test_person_list_filter_ge_int(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": 2, "op": "ge"}'))
    data = result['data']
    assert len(data) == 3


def test_person_list_filter_le(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": "2", "op": "le"}'))
    data = result['data']
    assert len(data) == 2


def test_person_list_filter_lt(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": "2", "op": "lt"}'))
    data = result['data']
    assert len(data) == 1


def test_person_list_filter_between(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": ["2", "4"], "op": "between"}'))
    data = result['data']
    assert len(data) == 3


def test_person_list_filter_in(client):
    result = get_json_result(client.get('/persons?filter={"name": "id", "val": ["1", "4"], "op": "in"}'))
    data = result['data']
    assert len(data) == 2


def test_person_list_filter_endswith(client):
    result = get_json_result(client.get('/persons?filter={"name": "lastName", "val": "mann", "op": "endswith"}'))
    data = result['data']
    assert len(data) == 2


def test_person_list_filter_recursive(client):
    # Test the following filter:
    fltr = {
        "op": "or",
        "val": [
            {
                "op": "in",
                "name": "id",
                "val": [
                    "1",
                    "2"
                ]
            },
            {
                "op": "notin",
                "name": "id",
                "val": [
                    "1",
                    "2"
                ]
            }
        ]
    }
    result = get_json_result(client.get(f'/persons?filter={json.dumps(fltr)}'))
    data = result['data']
    assert len(data) == 4


def test_person_list_filter_null_value(client):
    result = get_json_result(client.get('/persons?filter={"name": "firstName", "val": null, "op": "isnot"}'))
    data = result['data']
    assert len(data) == 4


def test_person_include_whitespace(client):
    result = get_json_result(client.get('/persons?include=organization,'))
    data = result['data']
    assert len(data) == 4


def test_person_list_wrong_filter(client):
    result = get_json_result(
        client.get('/persons?filter=[{"name": "firstName", "val": null, "op": "isnot"}'),
        status_code=422
    )

def test_person_filter_any(client):

    result = get_json_result(client.get('/persons?filter={"name": "hobbies", "op":"any", "val": {"name": "name", "op": "eq", "val": "football"}}'))
    data = result['data']
    assert len(data) == 2

def test_person_filter_any_wrong(client):
    get_json_result(client.get('/persons?filter={"name": "organization", "op":"any", "val": {"name": "name", "op": "eq", "val": "football"}}'), status_code=422)

def test_person_implpicit_join_wrong(client):
    get_json_result(client.get('/persons?filter={"name": "nonexisting.foo", "val": "wok"}'), status_code=422)


def test_person_filter_any_list(client):

    result = get_json_result(client.get('/persons?filter={"name": "hobbies", "op":"any", "val": [{"name": "id", "val": 3},{"name": "name", "val": "football"}]}'))
    data = result['data']
    assert len(data) == 2


def test_person_filter_wrong_entity(client):
    get_json_result(client.get('/persons?filter={"name": "hobbies.wrong", "val": "DUMMY"}'), status_code=422)


def test_person_filter_wrong_operator(client):
    get_json_result(client.get('/persons?filter={"name": "firstName", "op": "DUMMY", "val": "DUMMY"}'), status_code=400)

def test_filter_on_to_many(client):
    get_json_result(client.get('/persons?filter={"name": "hobbies.name", "val": "DUMMY"}'), status_code=422)

