from .sample_app.models import Person
from .utils import get_json_result


# @pytest.mark.asyncio
def test_person_get(client):
    result = get_json_result(client.get('/persons/1'))
    assert result == {
        'data': {
            'attributes': {'firstName': 'John', 'lastName': 'Doe'},
            'id': '1',
            'links': {'self': '/persons/1'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/1/hobbies',
                        'self': '/persons/1/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/1/organization',
                        'self': '/persons/1/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/1'}
    }


def test_person_get_include_organization(client):
    result = get_json_result(client.get('/persons/2?include=organization'))
    assert result == {
        'data': {
            'attributes': {'firstName': 'Anna', 'lastName': 'Doe'},
            'id': '2',
            'links': {'self': '/persons/2'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/2/hobbies',
                        'self': '/persons/2/relationships/hobbies'
                    }
                },
                'organization': {
                    'data': {'id': '1', 'type': 'Organization'},
                    'links': {
                        'related': '/persons/2/organization',
                        'self': '/persons/2/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'included': [{
            'attributes': {'name': 'First National'},
            'id': '1',
            'links': {
                'self': '/organizations/1'
            },
            'relationships': {
                'organization-members': {
                    'links': {
                        'related': '/organizations/1/organization-members',
                        'self': '/organizations/1/relationships/organization-members'
                    }
                }
            },
            'type': 'Organization'
        }],
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/2'}
    }


def test_org_get_paginated(client):
    """
    Relationship must page[limit], but ignore page[number]
    """

    # first page
    result = get_json_result(client.get('/organizations?include=organization-members:1&page[limit]=1'))
    assert result == {
        'data': [{
            'type': 'Organization',
            'id': '1',
            'attributes': {
                'name': 'First National'
            },
            'relationships': {
                'organization-members': {
                    'links': {
                        'related': '/organizations/1/organization-members',
                        'self': '/organizations/1/relationships/organization-members'
                    },
                    'data': [
                        {'type': 'Person', 'id': '1'},
                    ],
                    'meta': {'count': 2}
                }
            },
            'links': {'self': '/organizations/1'}
        }],
        'included': [{
            'type': 'Person',
            'id': '1',
            'attributes': {'lastName': 'Doe', 'firstName': 'John'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/1/hobbies',
                        'self': '/persons/1/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {'related': '/persons/1/organization', 'self': '/persons/1/relationships/organization'}
                }
            },
            'links': {'self': '/persons/1'}
        }],
        'meta': {'count': 3},
        'links': {
            'self': '/organizations',
            'next': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=1',
            'first': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=0',
            'last': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2'
        },
        'jsonapi': {'version': '1.0'}
    }

    # second page
    result = get_json_result(client.get('/organizations?include=organization-members:1&page[limit]=1&page[offset]=1'))
    assert result == {
        'data': [{
            'type': 'Organization',
            'id': '2',
            'attributes': {
                'name': 'National Trust'
            },
            'relationships': {
                'organization-members': {
                    'links': {
                        'related': '/organizations/2/organization-members',
                        'self': '/organizations/2/relationships/organization-members'
                    },
                    'data': [
                        {'type': 'Person', 'id': '3'},
                    ],
                    'meta': {'count': 1}
                }
            },
            'links': {'self': '/organizations/2'}
        }],
        'included': [{
            'type': 'Person',
            'id': '3',
            'attributes': {'lastName': 'Mustermann', 'firstName': 'Max'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/3/hobbies',
                        'self': '/persons/3/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {'related': '/persons/3/organization', 'self': '/persons/3/relationships/organization'}
                }
            },
            'links': {'self': '/persons/3'}
        }],
        'meta': {'count': 3},
        'links': {
            'self': '/organizations',
            'next': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2',
            'previous': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=0',
            'first': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=0',
            'last': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2'
        },
        'jsonapi': {'version': '1.0'}
    }

    # third page
    result = get_json_result(client.get('/organizations?include=organization-members:1&page[limit]=1&page[offset]=2'))
    assert result == {
        'data': [{
            'type': 'Organization',
            'id': '3',
            'attributes': {
                'name': 'Ascam Ltd'
            },
            'relationships': {
                'organization-members': {
                    'links': {
                        'related': '/organizations/3/organization-members',
                        'self': '/organizations/3/relationships/organization-members'
                    },
                    'data': [],
                    'meta': {'count': 0}
                }
            },
            'links': {'self': '/organizations/3'}
        }],
        'meta': {'count': 3},
        'links': {
            'self': '/organizations',
            'previous': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=1',
            'first': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=0',
            'last': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2'
        },
        'jsonapi': {'version': '1.0'}
    }

    # fourth page - does not exist
    result = get_json_result(client.get('/organizations?include=organization-members:1&page[limit]=1&page[offset]=3'))
    assert result == {
        'data': [],
        'meta': {'count': 3},
        'links': {
            'self': '/organizations',
            'previous': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2',
            'first': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=0',
            'last': '/organizations?include=organization-members%3A1&page%5Blimit%5D=1&page%5Boffset%5D=2'
        },
        'jsonapi': {'version': '1.0'}
    }


# @pytest.mark.asyncio
def test_person_get_double_include(client):
    result = get_json_result(client.get('/persons/2?include=organization.organization-members'))

    assert result == {
        'data': {
            'attributes': {'firstName': 'Anna', 'lastName': 'Doe'},
            'id': '2',
            'links': {'self': '/persons/2'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/2/hobbies',
                        'self': '/persons/2/relationships/hobbies'
                    }
                },
                'organization': {
                    'data': {'id': '1', 'type': 'Organization'},
                    'links': {
                        'related': '/persons/2/organization',
                        'self': '/persons/2/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'included': [{
            'attributes': {'firstName': 'John', 'lastName': 'Doe'},
            'id': '1',
            'links': {'self': '/persons/1'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/1/hobbies',
                        'self': '/persons/1/relationships/hobbies'
                    }
                },
                'organization': {
                    'links': {
                        'related': '/persons/1/organization',
                        'self': '/persons/1/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        }, {
            'attributes': {'name': 'First National'},
            'id': '1',
            'links': {
                'self': '/organizations/1'
            },
            'relationships': {
                'organization-members': {
                    'data': [{'id': '1', 'type': 'Person'},
                             {'id': '2', 'type': 'Person'}],
                    'links': {
                        'related': '/organizations/1/organization-members',
                        'self': '/organizations/1/relationships/organization-members'
                    },
                    'meta': {'count': 2},
                }
            },
            'type': 'Organization'
        }],
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/2'}
    }


# @pytest.mark.asyncio
def test_person_get_empty_include(client):
    result = get_json_result(client.get('/persons/4?include=organization'))

    assert result == {
        'data': {
            'attributes': {'firstName': 'Erika', 'lastName': 'Mustermann'},
            'id': '4',
            'links': {'self': '/persons/4'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/4/hobbies',
                        'self': '/persons/4/relationships/hobbies'
                    }
                },
                'organization': {
                    'data': None,
                    'links': {
                        'related': '/persons/4/organization',
                        'self': '/persons/4/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/4'}
    }


def test_person_serialize_deserialize():
    d = {
        'id': 1,
        'firstName': 'John',
        'lastName': 'Doe',
    }
    copy = Person.model_validate(d)
    assert copy.model_dump() == {
        'id': 1,
        'first_name': 'John',
        'last_name': 'Doe',
        'meta': None
    }


# @pytest.mark.asyncio
def test_org_get(client):
    result = get_json_result(client.get('/organizations/2?include=organization-members'))
    assert result == {
        'data': {
            'attributes': {'name': 'National Trust'},
            'id': '2',
            'links': {'self': '/organizations/2'},
            'relationships': {
                'organization-members': {
                    'data': [
                        {'id': '3', 'type': 'Person'},
                    ],
                    'links': {
                        'related': '/organizations/2/organization-members',
                        'self': '/organizations/2/relationships/organization-members'
                    },
                    'meta': {'count': 1}
                }
            },
            'type': 'Organization'
        },
        'included': [
            {
                'attributes': {'firstName': 'Max', 'lastName': 'Mustermann'},
                'id': '3',
                'links': {'self': '/persons/3'},
                'relationships': {
                    'hobbies': {
                        'links': {
                            'related': '/persons/3/hobbies',
                            'self': '/persons/3/relationships/hobbies'
                        }
                    },
                    'organization': {
                        # 'data': None,
                        'links': {
                            'related': '/persons/3/organization',
                            'self': '/persons/3/relationships/organization'
                        }
                    }
                },
                'type': 'Person'
            }
        ],
        'links': {
            'self': '/organizations/2'
        },
        'jsonapi': {'version': '1.0'},
        # 'meta': None
    }


# @pytest.mark.asyncio
def test_deep_included(client):
    result = get_json_result(client.get(
        '/persons/2?include=organization.organization-members.organization.organization-members,organization.organization-members.organization.organization-members'))
    assert result == {
        'data': {
            'attributes': {'firstName': 'Anna', 'lastName': 'Doe'},
            'id': '2',
            'links': {'self': '/persons/2'},
            'relationships': {
                'hobbies': {
                    'links': {
                        'related': '/persons/2/hobbies',
                        'self': '/persons/2/relationships/hobbies'
                    }
                },
                'organization': {
                    'data': {'id': '1', 'type': 'Organization'},
                    'links': {
                        'related': '/persons/2/organization',
                        'self': '/persons/2/relationships/organization'
                    }
                }
            },
            'type': 'Person'
        },
        'included': [
            {
                'attributes': {'firstName': 'John', 'lastName': 'Doe'},
                'id': '1',
                'links': {'self': '/persons/1'},
                'relationships': {
                    'hobbies': {
                        'links': {
                            'related': '/persons/1/hobbies',
                            'self': '/persons/1/relationships/hobbies'
                        }
                    },
                    'organization': {
                        'data': {'id': '1', 'type': 'Organization'},
                        'links': {
                            'related': '/persons/1/organization',
                            'self': '/persons/1/relationships/organization'
                        }
                    }
                },
                'type': 'Person'
            },
            {
                'attributes': {'name': 'First National'},
                'id': '1',
                'links': {'self': '/organizations/1'},
                'relationships': {
                    'organization-members': {
                        'data': [
                            {'id': '1', 'type': 'Person'},
                            {'id': '2', 'type': 'Person'}
                        ],
                        'links': {
                            'related': '/organizations/1/organization-members',
                            'self': '/organizations/1/relationships/organization-members'
                        },
                        'meta': {'count': 2}
                    }
                },
                'type': 'Organization'}
        ],
        'jsonapi': {'version': '1.0'},
        'links': {'self': '/persons/2'}
    }
