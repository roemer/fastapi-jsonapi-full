from abc import ABC, abstractmethod
from typing import Type

from fastapi import Request

from ..params.pagination import DEFAULT_PAGE_SIZE
from ..schema import JsonAPIEnvelope, JsonAPIEnvelopeMany
from ..model import Relationship, BaseModel, SerializationMode
from ..response import JsonAPIResponse
import fastapi_jsonapi_full.exceptions as exceptions


class BaseRequest(ABC):
    request: Request

    resource_mgr: 'BaseResource'

    include: str = None
    included_paths: dict

    envelope: JsonAPIEnvelope = None

    @abstractmethod
    def __call__(
            self,
            *args,
            **kwargs
    ):
        raise NotImplemented

    def _from_jsonapi_request(
            self,
            envelope: JsonAPIEnvelope,
            model: Type[BaseModel],
            serialization_mode: SerializationMode
    ) -> (BaseModel, dict[str, list[str]]):

        item_model = model.jsonapi_from_record(envelope.data, serialization_mode=serialization_mode)

        # get relationships
        record = envelope.data.model_dump(exclude_unset=True)
        jsonapi_relationships = record.pop('relationships', None) or {}
        relationships = {}

        for relationship_name, relationship_data in jsonapi_relationships.items():
            if 'data' in relationship_data:
                relation = model.__JSONAPI_RELATIONSHIP_DEFS__[relationship_name]
                relationships[relationship_name] = self._get_relationship_ids(
                    relation,
                    relationship_data.get('data'),
                    many=relation.many
                )

        return item_model, relationships

    def _from_jsonapi_relationships_request(
            self,
            envelope: JsonAPIEnvelope,
            relationship: Relationship,
            many: bool,
            convert_many: bool = False
    ) -> list:
        """
        Return a list of related ids from a JsonAPI envelope containing JsonAPI links
        """
        envelope_data = envelope.model_dump(exclude_unset=True)
        data = envelope_data.get('data')

        if convert_many:
            if many and not isinstance(data, list):
                data = [data]
            elif not many and isinstance(data, list) and len(data) == 1:
                data = data[0]

        return self._get_relationship_ids(relationship, data, many)

    def _get_relationship_ids(self, relationship: Relationship, data: dict | list | None, many: bool) -> list:
        """
        Read relationship objects from JsonAPI request
        """
        links = []
        if many:
            try:
                model = relationship.related_model.__jsonapi_record_link_many_model__.model_validate(data)
            except TypeError:
                raise exceptions.UnexpectedContent(expected='list', actual=data, relationship=relationship)
            links = model.root
        elif data:
            try:
                model = relationship.related_model.__jsonapi_record_link_model__(**data)
            except TypeError:
                raise exceptions.UnexpectedContent(
                    expected=relationship.related_model.__jsonapi_record_link_model__.__name__, actual=data,
                    relationship=relationship)
            links = [model]
        elif isinstance(data, list):
            raise exceptions.UnexpectedContent(expected='object or None', actual='list', relationship=relationship)

        return [link.id for link in links]

    def _get_included_relations(self, include: str, model: BaseModel) -> dict:
        """
        Create a dictionary of included relationships. The values are tuples (deeper_relation_dict, limit)

        Example: assuming `/persons/1?included=organization.members,organization.main_contact`, the following dictionary is returned:

        { RelationConfig[Organization] : ({ RelationConfig[Members]: ({},10), RelationConfig[MainContact]: ({},10) },10)

        :param included:
        :return:
        """

        included_paths = {}

        if include:
            for incl in include.split(','):
                incl = incl.strip()
                if incl:
                    part_dict = included_paths
                    model_type_name = model.__name__
                    parts = incl.split('.')
                    for part in parts:
                        if ':' in part:
                            relation_name, limit = part.split(':')
                            limit = int(limit)
                        else:
                            relation_name = part
                            limit = DEFAULT_PAGE_SIZE

                        relation_config = BaseModel.__MODEL_REGISTRY__[model_type_name].__JSONAPI_RELATIONSHIP_DEFS__.get(
                            relation_name)
                        if not relation_config:
                            raise exceptions.RelationshipNotFound(relationship=f"{model_type_name}.{relation_name}")

                        part_dict = part_dict.setdefault(relation_config.full_name, ({}, limit))[0]
                        model_type_name = relation_config.related_model_name

        return included_paths


def to_jsonapi_response(
        resource_mgr: 'ResourceManager',
        data: list[BaseModel] = None,
        registry: dict = None,
        links: dict = None,
        many: bool = False,
        meta: dict = None,
        status_code: int = 200,
        as_link_only: bool = False,
        include_main_relation: bool = False,
        include_data_if_empty: bool = True
) -> dict | JsonAPIResponse:

    envelope_dict = {
        'jsonapi': {
            'version': '1.0'
        }
    }

    models = []
    if data is not None:
        for rec in data:
            # pop main results from registry
            if not include_main_relation:
                registry[type(rec).__name__].pop(resource_mgr.data_layer.get_id(rec))

            # create JsonAPIRecords for main results
            # as_link_only means: only render a link, as is needed for relationship endpoints
            model = rec.jsonapi_to_record(resource_mgr.path)
            if as_link_only:
                models.append(model.model_dump(exclude_unset=True, by_alias=True, include={'id', 'type'}))
            else:
                models.append(model.model_dump(exclude_unset=True, by_alias=True))

        if many:
            envelope_dict['data'] = models
            # envelope = JsonAPIEnvelopeMany.model_validate(envelope_dict)
        elif models:
            envelope_dict['data'] = models[0]
            # envelope = JsonAPIEnvelope.model_validate(envelope_dict)
        elif include_data_if_empty:
            envelope_dict['data'] = None

        # create JsonAPIRecords for all other (included) results
        included = []
        for model_name, included_records in registry.items():
            included_resource_manager = resource_mgr.__RESOURCE_REGISTRY__[model_name]
            for rec in included_records.values():
                rec_model = rec.jsonapi_to_record(included_resource_manager.path).model_dump(exclude_unset=True,
                                                                                       by_alias=True)
                included.append(rec_model)

        if included:
            envelope_dict['included'] = included

    if meta:
        envelope_dict['meta'] = meta
    if links:
        envelope_dict['links'] = links

    if many:
        envelope = JsonAPIEnvelopeMany.model_validate(envelope_dict)
    else:
        envelope = JsonAPIEnvelope.model_validate(envelope_dict)

    return JsonAPIResponse(content=envelope, status_code=status_code)
