from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope
from ..model import Relationship, BaseModel, SerializationMode
from .base_request import BaseRequest, to_jsonapi_response


class RelatedPostRequest(BaseRequest):
    item_id: str

    relation: Relationship
    related_resource_mgr: 'BaseResource'

    related_item_model: BaseModel

    relationships: dict

    envelope: JsonAPIEnvelope = None

    def __init__(
            self,
            resource_mgr: 'BaseResource',
            relation_name: str
    ):
        self.resource_mgr = resource_mgr

        self.relation = resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        self.related_resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[self.relation.related_model_name]

    def __call__(
            self,
            request: Request,
            envelope: JsonAPIEnvelope,
            item_id: str = Path(..., alias='id'),
            include: str = Query(None),
    ):
        self.request = request
        self.item_id = item_id
        self.include = include
        self.envelope = envelope

        self.related_item_model, self.relationships = self._from_jsonapi_request(
            envelope,
            self.related_resource_mgr.model,
            serialization_mode=SerializationMode.POST
        )
        self.included_paths = self._get_included_relations(include, self.related_resource_mgr.model)

        return self

    def create_response(
            self,
            related_item_model: 'BaseModel',
            registry: dict[str, dict[str, 'BaseModel']],
            links: dict[str, str] = None,
            status_code: int = 201,
            meta: dict = None
    ):
        links = {
            'related': f"{self.resource_mgr.path}/{self.item_id}/relationships/{self.relation.relation_name}",
            'self': f"{self.resource_mgr.path}/{self.item_id}/{self.relation.relation_name}"
        } | (links or {})

        return to_jsonapi_response(
            self.related_resource_mgr,
            [related_item_model],
            registry,
            links,
            many=False,
            status_code=status_code,
            meta=meta
        )
