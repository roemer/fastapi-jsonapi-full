from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope
from ..model import Relationship, BaseModel, SerializationMode
from .base_request import BaseRequest, to_jsonapi_response

import fastapi_jsonapi_full.exceptions as exceptions


class RelationshipPostRequest(BaseRequest):
    item_id: str

    relation: Relationship
    related_resource_mgr: 'BaseResource'

    related_item_id: str

    relationships: dict

    envelope: JsonAPIEnvelope = None

    def __init__(
            self,
            resource_mgr: 'BaseResource',
            relation_name: str
    ):
        self.resource_mgr = resource_mgr

        self.relation = resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        self.related_resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[self.relation.related_model_name]

    def __call__(
            self,
            request: Request,
            envelope: JsonAPIEnvelope,
            item_id: str = Path(..., alias='id'),
            include: str = Query(None),
    ):
        self.request = request
        self.item_id = item_id
        self.include = include
        self.envelope = envelope

        related_ids = self._from_jsonapi_relationships_request(envelope, self.relation, many=False)

        included_paths = self._get_included_relations(include, self.resource_mgr.model)

        # include the main relation (eg. the organization for /persons/1/organization in the included,
        # but only if we're serializing as links (otherwise it is already present in the main data element)
        self.include_main_relation = (self.relation.full_name in included_paths)
        self.included_paths = included_paths.get(self.relation.full_name, ({},))[0]

        # there must be precisely 1 related_id
        related_ids_count = len(related_ids) if related_ids else 0
        if related_ids_count == 1:
            self.related_item_id = related_ids[0]
        else:
            raise exceptions.UnexpectedContent(expected=1, actual=related_ids_count, relationship=self.relation.relation_name)

        return self

    def create_response(
            self,
            item_model: 'BaseModel',
            registry: dict[str, dict[str, 'BaseModel']],
            links: dict[str, str] = None,
            meta: dict = None,
            status_code: int = 201
    ):
        links = {
            'self': f"{self.resource_mgr.path}/{self.item_id}/relationships/{self.relation.relation_name}",
            'related': f"{self.resource_mgr.path}/{self.item_id}/{self.relation.relation_name}"
        } | (links or {})

        return to_jsonapi_response(
            self.related_resource_mgr,
            [item_model],
            registry,
            links=links,
            meta=meta,
            status_code=status_code,
            include_data_if_empty=True,
            as_link_only=True,
            include_main_relation=self.include_main_relation
        )
