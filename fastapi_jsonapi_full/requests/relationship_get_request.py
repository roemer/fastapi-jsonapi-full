from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope, JsonAPIEnvelopeMany
from ..model import Relationship, BaseModel, SerializationMode
from ..params import DefaultPaginationParam

from .base_request import BaseRequest, to_jsonapi_response

import fastapi_jsonapi_full.exceptions as exceptions


class RelationshipGetRequest(BaseRequest):
    item_id: str

    relation: Relationship
    related_resource_mgr: 'BaseResource'

    # related_item_ids: list[str]

    relationships: dict

    include_main_relation: bool

    def __init__(
            self,
            resource_mgr: 'BaseResource',
            relation_name: str
    ):
        self.resource_mgr = resource_mgr

        self.relation = resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        self.related_resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[self.relation.related_model_name]

    def __call__(
            self,
            request: Request,
            item_id: str = Path(..., alias='id'),
            include: str = Query(None),
    ):
        self.request = request
        self.item_id = item_id
        self.include = include

        included_paths = self._get_included_relations(include, self.resource_mgr.model)

        # include the main relation (eg. the organization for /persons/1/organization in the included,
        # but only if we're serializing as links (otherwise it is already present in the main data element)
        self.include_main_relation = (self.relation.full_name in included_paths)
        self.included_paths = included_paths.get(self.relation.full_name, ({},))[0]

        return self

    def create_response(
            self,
            item_models: list['BaseModel'],
            registry: dict[str, dict[str, 'BaseModel']],
            links: dict[str, str] = None,
            meta: dict = None,
            status_code: int = 200
    ):
        links = {
            'self': f"{self.resource_mgr.path}/{self.item_id}/relationships/{self.relation.relation_name}",
            'related': f"{self.resource_mgr.path}/{self.item_id}/{self.relation.relation_name}"
        } | (links or {})

        return to_jsonapi_response(
            self.related_resource_mgr,
            item_models,
            registry,
            links=links,
            many=self.relation.many,
            meta=meta,
            as_link_only=True,
            status_code=status_code,
            include_data_if_empty=True,
            include_main_relation=self.include_main_relation
        )
