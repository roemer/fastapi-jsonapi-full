from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope, JsonAPIEnvelopeMany
from ..model import Relationship, BaseModel, SerializationMode

from .base_request import BaseRequest, to_jsonapi_response

import fastapi_jsonapi_full.exceptions as exceptions


class RelationshipDeleteRequest(BaseRequest):
    item_id: str

    relation: Relationship
    related_resource_mgr: 'BaseResource'

    related_item_ids: list[str]

    relationships: dict

    envelope: JsonAPIEnvelopeMany | JsonAPIEnvelope = None

    def __init__(
            self,
            resource_mgr: 'BaseResource',
            relation_name: str
    ):
        self.resource_mgr = resource_mgr

        self.relation = resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        self.related_resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[self.relation.related_model_name]

    def __call__(
            self,
            request: Request,
            envelope: JsonAPIEnvelopeMany | JsonAPIEnvelope,
            item_id: str = Path(..., alias='id'),
            include: str = Query(None),
    ):
        self.request = request
        self.item_id = item_id
        self.include = include
        self.envelope = envelope

        self.related_item_ids = self._from_jsonapi_relationships_request(
            envelope,
            self.relation,
            many=self.relation.many,
            convert_many=True
        )
        if not self.related_item_ids:
            raise exceptions.UnprocessableContent

        return self

    def create_response(
            self,
            links: dict[str, str] = None,
            meta: dict = None,
            status_code: int = 200
    ):
        links = {
            'self': f"{self.resource_mgr.path}/{self.item_id}/relationships/{self.relation.relation_name}",
            'related': f"{self.resource_mgr.path}/{self.item_id}/{self.relation.relation_name}"
        } | (links or {})

        return to_jsonapi_response(
            self.related_resource_mgr,
            [],
            {},
            links=links,
            meta=meta,
            status_code=status_code,
            include_data_if_empty=False,
            as_link_only=True,
        )
