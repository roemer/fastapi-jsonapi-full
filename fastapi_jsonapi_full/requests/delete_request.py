from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope
from ..model import Relationship, BaseModel, SerializationMode
from .base_request import BaseRequest, to_jsonapi_response


class DeleteRequest(BaseRequest):
    item_id: str

    relationships: dict

    # envelope: JsonAPIEnvelope = None

    def __init__(
            self,
            resource_mgr: 'BaseResource'
    ):
        self.resource_mgr = resource_mgr

    def __call__(
            self,
            request: Request,
            item_id: str = Path(..., alias='id'),
    ):
        self.request = request
        self.item_id = item_id

        return self

    def create_response(
            self,
            status_code: int = 200,
            meta: dict = None
    ):
        return to_jsonapi_response(
            self.resource_mgr,
            many=False,
            status_code=status_code,
            meta=meta
        )
