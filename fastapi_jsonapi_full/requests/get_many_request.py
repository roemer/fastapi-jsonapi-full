from typing import Type

from fastapi import Path, Query, Request


from ..schema import JsonAPIEnvelope
from ..model import Relationship, BaseModel, SerializationMode
from .base_request import BaseRequest, to_jsonapi_response


class GetManyRequest(BaseRequest):

    relationships: dict

    def __init__(
            self,
            resource_mgr: 'BaseResource'
    ):
        self.resource_mgr = resource_mgr

    def __call__(
            self,
            request: Request,
            include: str = Query(None),
    ):
        self.request = request
        self.include = include
        self.included_paths = self._get_included_relations(include, self.resource_mgr.model)
        return self

    def create_response(
            self,
            item_models: list['BaseModel'],
            registry: dict[str, dict[str, 'BaseModel']],
            links: dict[str, str] = None,
            status_code: int = 200,
            meta: dict = None
    ):
        links = {
            'self': f"{self.resource_mgr.path}"
        } | (links or {})

        return to_jsonapi_response(
            self.resource_mgr,
            item_models,
            registry,
            links,
            many=True,
            status_code=status_code,
            meta=meta
        )
