from typing import Type
import logging
from typing import Iterable, Any, Tuple
from sqlalchemy import func, text, union_all, select
from sqlalchemy.orm import Session, aliased, Query

from ..data_layer import BaseDataLayer as BaseBaseDataLayer
from ..model import Relationship
from ..params.pagination import DEFAULT_PAGE_SIZE, DefaultPaginationParam

from .filter import FilterParam
from .sort import SortParam
from .utils import get_attribute_or_column

logger = logging.getLogger(__name__)


class BaseDataLayer(BaseBaseDataLayer):

    orm_type: Type[object]
    """SqlAlchemy entity type"""

    orm_alias_mappings: dict[str | None, Type[aliased] | Type[object]] = None
    """
    Alias mappings for ORM objects for deeper filter/sort expressions
    
    The key of the dict is the name of the relationship.
    The main entity (the `orm_type`) should have the key `None`. 
    
    ```
    orm_alias_mappings = {
        None: aliased(orm_type),
        'organization': aliased(Organization)
    }
    ```
    
    It is generally not needed to use `aliased` for the main entity, 
    but it may be needed in cases where there are join paths to the main entity through (deeper) relations:
    
    ```
    orm_alias_mappings = {
        None: aliased(Person),
        'organization': aliased(Organization)
        'organization.manager': aliased(Person, 'manager')
    }
    ```
     
    
    """

    def get_alias_mappings(self) -> dict[str | None, Type[aliased] | Type[object]]:
        if not self.orm_alias_mappings:
            self.orm_alias_mappings = {
                None: self.orm_type
            }

        return self.orm_alias_mappings

    def get_default_sort(self) -> list[Any]:
        att = get_attribute_or_column(self.get_alias_mappings()[None], 'id')
        return [att.asc()]

    async def create_orm(self, session: Session = None, **kwargs) -> object:
        item = self.orm_type()
        session.add(item)
        return item

    async def delete_orm(self, item: object, session: Session = None, **kwargs):
        session.delete(item)

    def base_query(
            self,
            filter: FilterParam = None,
            sort: SortParam = None,
            **kwargs
    ) -> Query:
        """Create the base query, and apply sort and filter"""

        query = select(self.orm_type)
        query = self.apply_filter_and_sort(query, filter, sort)

        return query

    def apply_filter_and_sort(
            self,
            query: Query,
            filter: FilterParam = None,
            sort: SortParam = None,
    ) -> Query:

        # Create local copy to be amended with implicit joins
        mappings = dict(self.get_alias_mappings())

        if filter:
            filter_clauses = filter.get_clauses(self.model, mappings)
        else:
            filter_clauses = []

        if sort:
            sort_clauses = sort.get_clauses(self.model, mappings)
        else:
            sort_clauses = self.get_default_sort()

        # Implicit joins
        for k,v in mappings.items():
            if hasattr(v, 'needs_join'):
                attr = getattr(mappings[None], k)
                query = query.join(attr.of_type(v), isouter=True)

        query = query.filter(*filter_clauses)
        query = query.order_by(*sort_clauses)

        return query

    async def find_by_ids(
            self,
            ids: Iterable[str],
            session: Session = None,
            **kwargs
    ) -> list[object]:

        query = self.base_query(**kwargs)
        entity = self.get_alias_mappings()[None]
        query = query.filter(entity.id.in_(ids))

        return session.scalars(query).all()

    async def find_many(
            self,
            pagination: DefaultPaginationParam = None,
            filter: FilterParam = None,
            sort: SortParam = None,
            session: Session = None,
            **kwargs
    ) -> Tuple[list[object], int]:

        base_query = self.base_query(
            filter=filter,
            sort=sort,
            **kwargs
        )


        query = base_query.limit(pagination.limit).offset(pagination.offset)
        result = session.scalars(query).all()

        # do a count query if our result reach the limit
        if len(result) == pagination.limit:
            count_query = select(func.count()).select_from(base_query)
            count = session.execute(count_query).scalar()
        else:
            count = len(result) + pagination.offset

        return result, count

    async def find_related(
            self,
            foreign_keys: Iterable[Any],
            relation_config: Relationship,
            pagination: DefaultPaginationParam = None,
            filter: FilterParam = None,
            sort: SortParam = None,
            session: Session = None,
            **kwargs
    ) -> dict[Any, tuple[list[object], int]]:

        foreign_keys = tuple(foreign_keys)

        query = self.base_query(
            filter=filter,
            sort=sort,
            **kwargs
        )
        aliased_model = join_model = self.get_alias_mappings()[None]
        sort_clauses = (aliased_model.id.asc(),)

        if pagination is None:
            offset = 0
            limit = kwargs.get('limit', DEFAULT_PAGE_SIZE)
        else:
            offset = pagination.offset
            limit = pagination.limit

        if relation_config.secondary is not None:
            query = query.join(relation_config.secondary)
            join_model = relation_config.secondary

        foreign_key = get_attribute_or_column(join_model, relation_config.remote_side)

        full_query = query.add_columns(foreign_key.label('_fk'))
        query = full_query.filter(foreign_key.in_(foreign_keys))

        # do we need to construct a CTE?
        # if there is more than one FK, then we need a CTE query;
        # it also means that we do not need to use full pagination:
        # the offset is always zero, so we just use the limit

        if len(foreign_keys) > 1:
            # more than one FK, so we should use the CTE
            # offset is always 0
            cte_query = query.add_columns(func.row_number().over(partition_by=foreign_key, order_by=sort_clauses).label('_fk_row_number')).cte('cte')

            result_query = (
                select(aliased_model, cte_query.c._fk)
                .join(cte_query, cte_query.c.id == aliased_model.id)
                .filter(
                    cte_query.c._fk_row_number <= limit,
                )
                .order_by(*sort_clauses)
            )
        else:
            # normal pagination; we only need to apply this if we have a single FK
            result_query = query.limit(limit).offset(offset)


        # collect related objects in dict [foreign_key: result_list]
        result = session.execute(result_query).all()
        related = {}
        for row in result:
            # we assume first object is the one we need, and last is fk
            related_obj = row[0]
            fk = row['_fk']
            related.setdefault(fk, []).append(related_obj)

        # if we've received the limit we need to do a count query
        fk_needs_count = [fk for fk,elms in related.items() if len(elms) == limit]
        if fk_needs_count:
            # create subquery for counts
            count_subquery = full_query.filter(foreign_key.in_(fk_needs_count)).subquery('counts')
            count_query = select(count_subquery.c._fk, func.count(count_subquery.c._fk)).group_by(count_subquery.c._fk)
            count_result = session.execute(count_query).all()
            counts = {fk: _count for fk, _count in count_result}
        else:
            counts = {}

        result = {fk: (related.get(fk,[]), counts.get(fk, len(related.get(fk,[])) + offset)) for fk in foreign_keys}
        return result

    async def save(self, session: Session = None, **kwargs):
        """Save the current transaction (Unit of Work)"""
        session.commit()

    async def update_orm(self, item: object, model, patch: bool = False, session: Session = None, **kwargs):
        """Update the entity"""
        updates = model.model_dump(exclude_unset=patch)
        for field, value in updates.items():
            if field == 'id':
                # Only assign id when explicitly present
                continue

            setattr(item, field, value)

        session.add(item)

