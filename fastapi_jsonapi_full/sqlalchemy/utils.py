from typing import Any
import sqlalchemy as sa

from fastapi_jsonapi_full import BaseModel
from fastapi_jsonapi_full.exceptions import BadRequest


def get_attribute_or_column(orm_model, field_name: str) -> Any:
    # try to find the sort field as an attribute
    if hasattr(orm_model, field_name):
        attr = getattr(orm_model, field_name)
    else:
        # not found, try to find column instead
        attr = getattr(orm_model, 'columns', {}).get(field_name)

    if attr is None:
        raise BadRequest(f"Attribute or column '{field_name}' unknown")

    return attr

def get_or_create_mapping(mappings: dict[str | None, Any], path: str) -> BaseModel:
    """Finds the ORM mapping for the path. If it doesn't exist, create it as alias and mark it to be joined later"""

    if not path:
        return mappings[None]
    mapped_alias = mappings[None]
    for p in path.split('.'):
        if p not in mappings:
            # Relation is not mapped; create implicit mapping and mark for implicit joining
            alias = sa.orm.aliased(getattr(mapped_alias, p).mapper.class_)
            mapped_alias = mappings[p] = alias
            mapped_alias.needs_join = True
        else:
            mapped_alias = mappings[p]

    return mapped_alias
