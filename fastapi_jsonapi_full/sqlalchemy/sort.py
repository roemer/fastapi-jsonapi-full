from typing import Any, Type
import sqlalchemy as sa
from .. import exceptions
from ..model import BaseModel
from ..params import SortParam as BaseSortParam

from .utils import get_attribute_or_column, get_or_create_mapping


class SortParam(BaseSortParam):

    def get_clauses(self, model: Type[BaseModel], mappings: dict[str | None, Any]) -> list[Any]:
        clauses = []

        for sort_clause in self.parse(model):

            orm_model = get_or_create_mapping(mappings,sort_clause.path)

            # try to find the sort field as an attribute
            attr = get_attribute_or_column(orm_model, sort_clause.field_name)

            clause = attr.asc() if sort_clause.ascending else attr.desc()
            clauses.append(clause)

        return clauses
