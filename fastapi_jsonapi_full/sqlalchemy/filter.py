from typing import Any, Type

import sqlalchemy as sa

from .. import exceptions
from ..model import BaseModel
from ..params import FilterParam as BaseFilterParam, FilterClause
from .utils import get_attribute_or_column, get_or_create_mapping
from ..convert_case import to_snake_case


def _convert_val(fltr, val):
    """Extract value, 'convert' through model"""
    if val is None:
        # None is not converted
        return None

    return next(iter(fltr.model.__jsonapi_filter_model__.model_validate({fltr.field.alias: val}).model_dump(
        exclude_unset=True).values()))


def _convert_list_val(fltr, val):
    """Extract list value, 'convert' through model"""
    return [_convert_val(fltr, v) for v in val]


class FilterParam(BaseFilterParam):
    """Filter parameter handling"""

    def get_clauses(self, model: Type[BaseModel], mappings: dict[str | None, Any]) -> list[Any]:
        fltr = self.parse(model)
        if fltr:
            return [self._create(fltr, mappings)]
        else:
            return [1 == 1]

    def _create(self, filter: FilterClause, mappings: dict[str | None, Any]):
        if filter.op == 'and':
            criteria = [self._create(fltr, mappings) for fltr in filter.filters]
            return sa.and_(*criteria)
        elif filter.op == 'or':
            criteria = [self._create(fltr, mappings) for fltr in filter.filters]
            return sa.or_(*criteria)
        elif filter.op == 'not':
            fltr = self._create(filter.filter, mappings)
            return sa.not_(fltr)
        elif filter.op == 'any':
            att_name = to_snake_case(filter.path)
            sa_relation = get_attribute_or_column(mappings[None], att_name)

            # create local scope
            inner_mappings = {
                None: sa_relation.mapper.class_
            }
            fltr = self._create(filter.filter, inner_mappings)
            return sa_relation.any(fltr)

        # normal ops
        mapped_entity = get_or_create_mapping(mappings,filter.path)

        # try to find the sort field as an attribute
        attr = get_attribute_or_column(mapped_entity, filter.field_name)

        match filter.op:
            case 'eq':
                # eq: check if field is equal to something
                return attr == _convert_val(filter, filter.val)
            case 'ieq':
                # ieq: case insensitive variant of eq
                return sa.func.lower(attr) == sa.func.lower(_convert_val(filter, filter.val))
            case 'ne':
                # ne: check if field is not equal to something
                return attr != _convert_val(filter, filter.val)
            case 'gt':
                # gt: check if field is greater than to something
                return attr > _convert_val(filter, filter.val)
            case 'ge':
                # ge: check if field is greater than or equal to something
                return attr >= _convert_val(filter, filter.val)
            case 'lt':
                # lt: check if field is less than to something
                return attr < _convert_val(filter, filter.val)
            case 'le':
                # le: check if field is less than or equal to something
                return attr <= _convert_val(filter, filter.val)
            case 'between':
                # between: used to filter a field between two values
                val1, val2 = _convert_list_val(filter, filter.val)
                return attr.between(val1, val2)
            # case 'any':
            #     # any: used to filter on to many relationships
            #     return sa.any_(attr) == val
            # case 'all':
            #     # all: used to filter on to many relationships
            #     return sa.all_(attr) == val
            case 'in':
                # in_: check if field is in a list of values
                return attr.in_(_convert_list_val(filter, filter.val))
            case 'endswith':
                # endswith: check if field ends with a string
                return attr.endswith(_convert_val(filter, filter.val))
            # case 'has':
            #     # has: used to filter on to one relationships
            #     return sa.has_(attr) == val
            case 'ilike':
                # ilike: check if field contains a string (case insensitive)
                return attr.ilike(_convert_val(filter, filter.val))
            case 'is':
                # is_: check if field is a value
                return attr.is_(_convert_val(filter, filter.val))
            case 'isnot':
                # isnot: check if field is not a value
                return attr.isnot(_convert_val(filter, filter.val))
            case 'like':
                # like: check if field contains a string
                return attr.like(_convert_val(filter, filter.val))
            case 'match':
                # match: check if field match against a string or pattern
                return attr.match(_convert_val(filter, filter.val))
            case 'notilike':
                # notilike: check if field does not contain a string (case insensitive)
                return attr.notilike(_convert_val(filter, filter.val))
            case 'notin':
                # notin_: check if field is not in a list of values
                return attr.notin_(_convert_list_val(filter, filter.val))
            case 'notlike':
                # notlike: check if field does not contain a string
                return attr.notlike(_convert_val(filter, filter.val))
            case 'startswith':
                # startswith: check if field starts with a string
                return attr.startswith(_convert_val(filter, filter.val))

        raise exceptions.BadRequest(f"Unknown operator '{filter.op}'")



