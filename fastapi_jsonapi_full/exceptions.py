from typing import Any, Union, Optional, Dict
import fastapi

from .response import JsonAPIResponse
from .schema import JsonAPIError, JsonAPIEnvelopeError


class HTTPException(fastapi.HTTPException):

    title: str = None
    code: str = None

    def __init__(
            self,
            status_code: int,
            detail: str = None,
            title: str = None,
            headers: Dict[str, Any] = None
    ):
        self.code = type(self).__name__
        self.title = title or self.code
        super().__init__(detail=detail, status_code=status_code, headers=headers)

    def to_jsonapi_response(self):
        error = JsonAPIError(
            status=str(self.status_code),
            code=self.code,
            title=self.title,
            detail=self.detail
        )
        envelope = JsonAPIEnvelopeError(errors=[error])
        return JsonAPIResponse(content=envelope, status_code=self.status_code)


class BadRequest(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Bad request',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=400, detail=detail, title=title, headers=headers)


class Forbidden(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Forbidden',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=403, detail=detail, title=title, headers=headers)


class NotFound(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Not found',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=404, detail=detail, title=title, headers=headers)


class MethodNotAllowed(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Method not allowed',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=405, detail=detail, title=title, headers=headers)


class Conflict(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Conflict',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=409, detail=detail, title=title, headers=headers)


class UnprocessableContent(HTTPException):
    def __init__(
            self,
            detail: Any = None,
            title: str = 'Unprocessable content',
            headers: Dict[str, Any] = None
    ):
        super().__init__(status_code=422, detail=detail, title=title, headers=headers)


class RelationshipConflict(UnprocessableContent):
    def __init__(self, relationship: Union['Relationship', str], title: str = 'Relationship not found', **kwargs):
        super().__init__(detail=f"Relationship conflict '{relationship}'", title=title, **kwargs)


class RelationshipNotFound(UnprocessableContent):
    def __init__(self, relationship: Union['Relationship', str], title: str = 'Relationship not found', **kwargs):
        super().__init__(detail=f"Unknown relationship '{relationship}'", title=title, **kwargs)


class UnexpectedContent(UnprocessableContent):
    def __init__(self, expected: Any, actual: Any, relationship: Union['Relationship', str] = None, title: str = 'Unexpected content', **kwargs):
        if relationship:
            detail = f"Expected {expected}, but found {actual} for relation '{relationship}'"
        else:
            detail = f"Expected {expected}, but found {actual}"
        super().__init__(detail=detail, title=title, **kwargs)


class UnexpectedType(UnprocessableContent):
    def __init__(self, expected: Any, actual: Any, relationship: Union['Relationship', str] = None, title: str = 'Unexpected type', **kwargs):
        if relationship:
            detail = f"Expected type {expected}, but found type {actual} for relation '{relationship}'"
        else:
            detail = f"Expected type {expected}, but found type {actual}"
        super().__init__(detail=detail, title=title, **kwargs)


class ItemNotFound(NotFound):
    def __init__(self, item_id: Any, relationship: Union['Relationship', str] = None, title: str = 'Item not found', **kwargs):
        if relationship:
            detail = f"Item with id '{item_id}' not found for relationship '{relationship}'"
        else:
            detail = f"Item with id '{item_id}' not found"
        super().__init__(detail=detail, title=title, **kwargs)


class InternalServerError(HTTPException):
    def __init__(self, detail: Any = None, title: str = 'Internal server error', **kwargs):
        super().__init__(status_code=500, detail=detail, title=title, **kwargs)


class TooManyResults(InternalServerError):
    def __init__(self, detail: Any = 'Too many results: one expected, more than one found', **kwargs):
        super().__init__(detail=detail, **kwargs)

