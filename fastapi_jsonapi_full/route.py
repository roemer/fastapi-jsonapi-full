from typing import Callable, List
import logging

from starlette.exceptions import HTTPException as StarletteException
import fastapi
import pydantic

from .exceptions import HTTPException, InternalServerError, UnprocessableContent

logger = logging.getLogger(__name__)


class JsonAPIRoute(fastapi.routing.APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: fastapi.Request) -> fastapi.Response:
            try:
                return await original_route_handler(request)
            # except RequestValidationError as exc:
            #     body = await request.body()
            #     detail = {"errors": exc.errors(), "body": body.decode()}
            #     # raise HTTPException(status_code=422, detail=detail)
            #     return JsonAPIResponse

            except HTTPException as ex:
                return ex.to_jsonapi_response()
            except pydantic.ValidationError as ex:
                js_ex = UnprocessableContent(detail=str(ex))
                return js_ex.to_jsonapi_response()
            except StarletteException as ex:
                http_ex = HTTPException(detail=ex.detail, status_code=ex.status_code, headers=ex.headers)
                return http_ex.to_jsonapi_response()
            except fastapi.exceptions.RequestValidationError as ex:
                js_ex = UnprocessableContent(detail=str(ex))
                return js_ex.to_jsonapi_response()
            except Exception as ex:
                logger.exception(ex)
                js_ex = InternalServerError(detail=str(ex))
                return js_ex.to_jsonapi_response()

        return custom_route_handler
