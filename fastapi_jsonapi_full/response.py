from fastapi import Response

from .schema import (
    JsonAPIEnvelope,
    JsonAPIEnvelopeMany,
    JsonAPIEnvelopeError,
    JsonAPIInfo
)


JSONAPI_MIME_TYPE = 'application/vnd.api+json'

JSONAPI_INFO = JsonAPIInfo(version='1.0')


class JsonAPIResponse(Response):
    media_type = JSONAPI_MIME_TYPE

    def render(self, envelope: JsonAPIEnvelope | JsonAPIEnvelopeMany | JsonAPIEnvelopeError) -> bytes:
        envelope.jsonapi = envelope.jsonapi or JSONAPI_INFO
        return envelope.json(exclude_unset=True).encode('utf-8')
