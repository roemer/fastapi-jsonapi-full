from typing import Any, Tuple

from .params import DefaultPaginationParam, SortParam, FilterParam
from .model import Relationship
from .utils import (
    append_relationship,
    create_item,
    find_item_by_id,
    find_items_by_ids,
    get_related_items,
    get_related_objects_sorted,
    process_included,
    remove_relationship,
    update_relationship,
    update_relationships,
)

import fastapi_jsonapi_full.requests as requests
import fastapi_jsonapi_full.exceptions as exceptions


async def delete(request: requests.DeleteRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)
    await request.resource_mgr.data_layer.delete_orm(item, **kwargs)
    await request.resource_mgr.data_layer.save(**kwargs)

    # return jsonapi_model
    return request.create_response()


async def get(request: requests.GetRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    item_model = request.resource_mgr.model.from_orm(item)
    registry = {request.resource_mgr.model.__name__: {request.resource_mgr.data_layer.get_id(item): item_model}}
    await process_included(request.resource_mgr, [item], registry, request.included_paths, **kwargs)

    return request.create_response(
        request.item_id,
        item_model,
        registry
    )


async def get_many(
        request: requests.get_many_request,
        pagination: DefaultPaginationParam,
        filter: FilterParam,
        sort: SortParam,
        **kwargs
):
    # we need to re-use the result, so convert to a list if this is an iterable
    data, count = await request.resource_mgr.data_layer.find_many(
        pagination=pagination,
        filter=filter,
        sort=sort,
        **kwargs
    )

    # find related data
    item_models = {request.resource_mgr.data_layer.get_id(o): request.resource_mgr.model.from_orm(o) for o in data}
    registry = {request.resource_mgr.model.__name__: item_models}
    await process_included(
        request.resource_mgr,
        data,
        registry,
        request.included_paths,
        pagination=DefaultPaginationParam(limit=pagination.limit),
        **kwargs
    )

    # calculate links
    pagination.set_count(count)
    links = {'self': f"{request.resource_mgr.path}"} | pagination.get_links()
    meta = pagination.get_meta()

    return request.create_response(
        list(item_models.values()),
        registry,
        links=links,
        meta=meta,
    )


async def patch(request: requests.PatchRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)
    await request.resource_mgr.data_layer.update_orm(item, request.item_model, patch=True, **kwargs)

    # retrieve relationships
    for relation_name, related_ids in request.relationships.items():
        # retrieve related objects
        relation = request.resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        resource_mgr = request.resource_mgr.__RESOURCE_REGISTRY__[relation.related_model_name]
        await update_relationship(request.resource_mgr, item, related_ids, relation, resource_mgr, **kwargs)

    await request.resource_mgr.data_layer.save(**kwargs)

    # find related data
    item_model = request.resource_mgr.model.from_orm(item)
    registry = {request.resource_mgr.model.__name__: {request.resource_mgr.data_layer.get_id(item): item_model}}
    await process_included(request.resource_mgr, [item], registry, request.included_paths, **kwargs)

    return request.create_response(
        request.item_id,
        item_model,
        registry
    )


async def post(request: requests.PostRequest = None, **kwargs):
    item = await create_item(request.resource_mgr, request.item_model, request.relationships, **kwargs)

    # find related data
    item_model = request.resource_mgr.model.from_orm(item)
    item_id = request.resource_mgr.data_layer.get_id(item)
    registry = {request.resource_mgr.model.__name__: {item_id: item_model}}
    await process_included(request.resource_mgr, [item], registry, request.included_paths, **kwargs)

    return request.create_response(
        item_id,
        item_model,
        registry,
        status_code=201
    )


async def related_get(
        request: requests.RelatedGetRequest,
        pagination: DefaultPaginationParam,
        filter: FilterParam,
        sort: SortParam,
        **kwargs
):
    root_item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    # get related objects through FK
    related_items, count = await get_related_items(
        root_item,
        request.relation,
        request.related_resource_mgr,
        pagination,
        filter=filter,
        sort=sort,
        **kwargs
    )

    related_item_models = {request.related_resource_mgr.data_layer.get_id(o): request.related_resource_mgr.model.from_orm(o) for o in related_items}
    registry = {request.related_resource_mgr.model.__name__: related_item_models}
    await process_included(
        request.related_resource_mgr,
        related_items,
        registry,
        request.included_paths,
        pagination=DefaultPaginationParam(limit=pagination.limit),
        **kwargs
    )

    meta = None
    links = {}

    if request.relation.many and pagination:
        pagination.set_count(count)
        links |= pagination.get_links()
        meta = pagination.get_meta()

    return request.create_response(
        list(related_item_models.values()),
        registry,
        links=links,
        meta=meta,
    )


async def related_post(request: requests.RelatedPostRequest = None, **kwargs):

    if request.relation.reverse not in request.relation.related_model.__JSONAPI_RELATIONSHIP_DEFS__:
        raise exceptions.RelationshipNotFound(relationship=request.relation.reverse)
    if request.relation.reverse in request.relationships.keys():
        raise exceptions.RelationshipConflict(relationship=request.relation.reverse)

    request.relationships[request.relation.reverse] = [request.item_id]
    related_item = await create_item(request.related_resource_mgr, request.related_item_model, request.relationships, **kwargs)

    related_item_model = request.related_resource_mgr.model.from_orm(related_item)
    registry = {request.related_resource_mgr.model.__name__: {request.related_resource_mgr.data_layer.get_id(related_item): related_item_model}}

    await process_included(request.related_resource_mgr, [related_item], registry, request.included_paths, **kwargs)

    return request.create_response(
        related_item_model,
        registry
    )


async def relationship_delete(request: requests.RelationshipDeleteRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    await remove_relationship(request.resource_mgr, item, request.related_item_ids, request.relation, request.related_resource_mgr, **kwargs)
    await request.resource_mgr.data_layer.save(**kwargs)

    return request.create_response()


async def relationship_get(
        request: requests.RelationshipGetRequest,
        pagination: DefaultPaginationParam,
        filter: FilterParam,
        sort: SortParam,
        **kwargs
):
    root_item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    # get related objects through FK
    related_items, count = await get_related_items(
        root_item,
        request.relation,
        request.related_resource_mgr,
        pagination=pagination,
        filter=filter,
        sort=sort,
        **kwargs
    )

    related_item_models = {request.related_resource_mgr.data_layer.get_id(o): request.related_resource_mgr.model.from_orm(o) for o in related_items}
    registry = {request.related_resource_mgr.model.__name__: related_item_models}
    await process_included(
        request.related_resource_mgr,
        related_items,
        registry,
        request.included_paths,
        pagination=DefaultPaginationParam(limit=pagination.limit),
        **kwargs
    )

    meta = None
    links = {}

    if request.relation.many and pagination:
        pagination.set_count(count)
        links |= pagination.get_links()
        meta = pagination.get_meta()

    return request.create_response(
        list(related_item_models.values()),
        registry,
        links=links,
        meta=meta,
    )


async def relationship_patch(request: requests.RelationshipPatchRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    related_items = await update_relationship(request.resource_mgr, item, request.related_item_ids, request.relation, request.related_resource_mgr, **kwargs)
    await request.resource_mgr.data_layer.save(**kwargs)

    related_item_models = {request.related_resource_mgr.data_layer.get_id(o): request.related_resource_mgr.model.from_orm(o) for o in related_items}
    registry = {request.related_resource_mgr.model.__name__: related_item_models}
    await process_included(
        request.related_resource_mgr,
        related_items,
        registry,
        request.included_paths,
        **kwargs
    )

    return request.create_response(
        list(related_item_models.values()),
        registry
    )


async def relationship_post(request: requests.RelationshipPostRequest = None, **kwargs):
    item = await find_item_by_id(request.resource_mgr, request.item_id, **kwargs)

    related_item = await append_relationship(
        request.resource_mgr,
        item,
        request.related_item_id,
        request.relation,
        request.related_resource_mgr,
        **kwargs
    )
    await request.resource_mgr.data_layer.save(**kwargs)

    related_item_model = request.related_resource_mgr.model.from_orm(related_item)
    related_item_models = {request.related_resource_mgr.data_layer.get_id(related_item): related_item_model}
    registry = {request.related_resource_mgr.model.__name__: related_item_models}
    await process_included(
        request.related_resource_mgr,
        [related_item],
        registry,
        request.included_paths,
        **kwargs
    )

    return request.create_response(
        related_item_model,
        registry
    )

