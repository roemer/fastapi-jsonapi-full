import humps


def to_snake_case(name: str) -> str:
    """Return 'string_in_snake_case'"""
    return humps.kebabize(name).replace('-', '_').lower()


def to_kebab_case(name: str) -> str:
    """Return 'string-in-kebab-case'"""
    return humps.kebabize(name).lower()


def to_pascal_case(name: str) -> str:
    """Return 'StringInPascalCase'"""
    return humps.pascalize(humps.kebabize(name).replace('-', '_').lower())


def to_camel_case(name: str) -> str:
    """Return 'stringInCamelCase'"""
    return humps.camelize(humps.kebabize(name).replace('-', '_').lower())
