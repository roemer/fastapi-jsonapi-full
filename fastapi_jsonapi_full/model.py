from typing import Type, Optional, Literal, Union, ClassVar
from copy import copy
from enum import Enum

import pydantic as pd

import logging

from .exceptions import UnexpectedType
from .schema import (
    JsonAPIRecord,
    JsonAPILinks,
    JsonAPIRelationship,
)
from .convert_case import to_kebab_case, to_camel_case, to_snake_case

logger = logging.getLogger(__name__)


REF_TEMPLATE = '#/components/schemas/{model}'


class _Generated:
    """Helper class for distinguishing generated models from user defined models"""
    __jsonapi__generated__: bool = True
    pass


ATTRIBUTE_CONFIG = pd.ConfigDict(
    from_attributes=True,
    populate_by_name=True,
)


ALIAS_GENERATOR = pd.AliasGenerator(
    alias=to_camel_case,
)


class SerializationMode(Enum):
    DEFAULT = 'default'
    PATCH = 'patch'
    POST = 'post',
    RELATIONSHIPS = 'relationships'


class Relationship:
    """
    JsonAPI relationship
    """

    __RELATIONSHIP_REGISTRY__: dict[str, 'Relationship'] = {}
    """Registry of all relationships"""

    related_model_name: str = None
    """Name of the related model"""

    relation_name: str = None
    """Name of the relation"""

    reverse: str = None
    """Name of the reverse relation. required for handling related calls"""

    attribute_name: str = None
    """Name of the data layer attribute; defaults to relation_name"""

    foreign_key: str = None
    """Name of the foreign key"""

    remote_side: str = None
    """Name of the remote side identifier, ie. the identifier the foreign key refers to"""

    secondary: str = None
    """Secondary table (link table), if used"""

    owner: Type['BaseModel'] = None
    """Owner model"""

    many: bool = None
    """True if this is a to-many relationship, otherwise False"""

    @property
    def full_name(self) -> str:
        """Full name of the Relationship"""
        return f"{self.owner.__name__}.{self.relation_name}"

    @property
    def related_model(self) -> Type['BaseModel']:
        """Returns the related model"""
        return BaseModel.__MODEL_REGISTRY__[self.related_model_name]

    def __init__(
            self,
            related_model: str | Type['BaseModel'],
            foreign_key: str,
            remote_side: str = None,
            many: bool = False,
            attribute_name: str = None,
            secondary: str = None,
            **kwargs
    ):
        self.related_model_name = related_model if isinstance(related_model, str) else related_model.__name__
        self.foreign_key = foreign_key
        self.remote_side = remote_side
        self.many = many or secondary is not None
        self.attribute_name = attribute_name
        self.secondary = secondary

        for att, val in kwargs.items():
            setattr(self, att, val)

    def __set_name__(self, owner, name):
        self.relation_name = name
        self.attribute_name = self.attribute_name or name

        relationships = getattr(owner, '_relationships', [])
        relationships.append(self)
        setattr(owner, '_relationships', relationships)

    def register_owner(self, model_cls: Type['BaseModel']):
        self.owner = model_cls
        self.relation_name = to_kebab_case(self.relation_name)
        logger.debug(f"Initializing relationship {self.full_name}")
        Relationship.__RELATIONSHIP_REGISTRY__[self.full_name] = self

    def __repr__(self):
        return self.full_name

    def __str__(self):
        return self.full_name


class BaseModel(pd.BaseModel):
    ##############################
    # static attributes

    __MODEL_REGISTRY__: ClassVar[dict[str, Type['BaseModel']]] = {}
    """Registry of all models"""

    __JSONAPI_RELATIONSHIP_DEFS__: ClassVar[dict[str, Relationship]] = None
    """
    The Relationship definitions for this model
    """

    _jsonapi_record_models: ClassVar[dict[str, Type['JsonAPIRecord']]] = {}
    """Registry of all JsonAPI models"""

    _jsonapi_schemas: ClassVar[dict[str, dict]] = {}
    _jsonapi_schema_definitions: ClassVar[dict[str, dict]] = {}

    _includes_model: ClassVar[pd.BaseModel] = None
    """Model of included JsonAPIRecord models"""

    _initialized: ClassVar[bool] = False
    """Flag to indicate the models have initialized"""

    ##############################
    # instance attributes

    _relationships: dict[str, list[str]] = pd.PrivateAttr({})
    """Relationship registry"""

    _relationship_counts: dict[str, int] = pd.PrivateAttr({})
    """Relationship count registry"""

    ##############################
    # class attributes

    _jsonapi_patch_model: ClassVar[Type['BaseModel']] = None
    """
    The PATCH model

    All fields other than id are marked optional, defaults are removed, validation is kept.
    """

    _jsonapi_post_model: ClassVar[Type['BaseModel']] = None
    """
    The POST model

    Field id is marked as optional.
    """

    __jsonapi_filter_model__: ClassVar[Type['BaseModel']] = None
    """
    The filter model

    This is for processing filter arguments; all fields are marked optional, defaults are removed, validation is removed.
    """

    # __jsonapi_record_model__: Type[JsonAPIRecord] = None
    """
    The JsonAPIRecord model for this model
    """

    __jsonapi_record_link_model__: ClassVar[Type[JsonAPIRecord]] = None
    """
    The JsonAPIRecord ref (link) model for this model
    """

    __jsonapi_record_link_many_model__: ClassVar[Type[JsonAPIRecord]] = None
    """
    The JsonAPIRecord ref (link) list model for this model
    """

    meta: Optional[dict] = None
    """
    JsonAPIRecord metadata.
    
    Data is a dictionary (Json dict), expected in the following format:
    
    ```
    self: dict 
    [RELATIONSHIP]: dict
    ```
    
    `self` metadata is passed in the record itself, 
    `[RELATIONSHIP]` metadata is included in the `relationships` element:
    
    Example:
    
    ```
    {
        'self': {
            'permissions': {
                'update': True
            }
        },
        'organization': {
            'permissions': {
                'create': True
            }
        }
    }
    ```

    renders as:
    
    ```
    {
        'meta': {
            'permissions': {
                'update': True
            }
        },
        'relationships': {
            'organization': {
                'meta': {
                    'permissions': {
                        'create': True
                    }
                }
            }
        }
    }    
    ```
    """

    model_config = pd.ConfigDict(
        from_attributes=True,
        alias_generator=ALIAS_GENERATOR,
        populate_by_name=True,
    )

    @classmethod
    def _generate_jsonapi_record_model(cls) -> Type[JsonAPIRecord]:
        model = BaseModel._jsonapi_record_models.get(cls.__name__)
        # model = cls.__jsonapi_record_model__
        if not model:
            logger.debug(f"Generating {cls.__name__}")
            type_name = cls.__name__

            atts = {}
            for field_name, field in cls.model_fields.items():
                # skip the id and meta fields
                if field_name == 'id' or field_name == 'meta':
                    continue

                field_info = copy(field)
                field_info.default = None
                field_info.default_factory = None
                atts[field_name] = (field.annotation, field_info)

            atts_name = f"{cls.__name__}Attributes"
            atts_model = pd.create_model(atts_name, __config__=ATTRIBUTE_CONFIG, **atts)
            fields = {
                'type': (Literal[type_name], ...),
                'id': (Optional[str], None),
                'attributes': (Optional[atts_model], pd.Field(None)),
                'links': (Optional[JsonAPILinks], pd.Field(None)),
                'relationships': (Optional[cls._generate_jsonapi_relationships_model()], pd.Field(None))
            }

            # We do not (for now) type the meta=data,
            # as this interferes with passing on the metadata to
            # the relationships.

            # meta_field = cls.__fields__.get('meta')
            # if meta_field:
            #     field_info = meta_field.field_info
            #     field_info.default = None
            #     field_info.default_factory = None
            #     field_info.alias = 'meta'
            #     fields['meta'] = (meta_field.annotation, field_info)

            model = pd.create_model(f"{cls.__name__}", __base__=JsonAPIRecord, **fields)
            BaseModel._jsonapi_record_models[cls.__name__] = model

        return model

    @classmethod
    def _generate_jsonapi_record_link_models(cls) -> (Type[pd.BaseModel], Type[pd.RootModel]):
        logger.debug(f"Generating {cls.__name__}Link")
        fields = {
            'type': (Literal[cls.__name__], ...),
            'id': (str, ...),
        }

        model = pd.create_model(f"{cls.__name__}Link", **fields)

        # generate list (many model)
        fields = {
            'root': (list[model], ...)
        }
        many_model = pd.create_model(f"{cls.__name__}LinkList", __base__=pd.RootModel, **fields)
        return model, many_model

    @classmethod
    def _generate_jsonapi_relationships_model(cls) -> Type[pd.BaseModel]:
        logger.debug(f"Generating {cls.__name__}Relationships")
        fields = {}
        for relation_name, relation_def in cls.__JSONAPI_RELATIONSHIP_DEFS__.items():
            # class JsonAPIRelationship(pd.BaseModel):
            #     links: Optional[JsonAPILinks]
            #     data: Optional[JsonAPIRecordLink] | Optional[list[JsonAPIRecordLink]]
            #     meta: Optional[dict]

            relationship_fields = {
                'data': (
                    Optional[list[relation_def.related_model.__jsonapi_record_link_model__]], pd.Field(None)
                ) if relation_def.many
                else (
                    Optional[relation_def.related_model.__jsonapi_record_link_model__], pd.Field(None)
                )
            }
            relationship_model = pd.create_model(f"{cls.__name__}{to_snake_case(relation_def.relation_name)}Relationship", __base__=JsonAPIRelationship, **relationship_fields)
            fields[relation_name] = (Optional[relationship_model], pd.Field(None))

        relationship_model = pd.create_model(f"{cls.__name__}Relationships", **fields)
        return relationship_model

    @classmethod
    def _generate_patch_model(cls) -> Type[pd.BaseModel]:
        """
        Create a PATCH model: make the fields other than id optional, remove defaults
        """
        logger.debug(f"Generating {cls.__name__}__PATCH")
        model = pd.create_model(f"{cls.__name__}__PATCH", __base__=(cls, _Generated))

        model.model_fields.pop("meta")

        for field_name, field in model.model_fields.items():
            if field_name != 'id':
                field.default = None
                field.default_factory = None

        model.model_rebuild(force=True)

        return model

    @classmethod
    def _generate_post_model(cls) -> Type[pd.BaseModel]:
        """
        Create a POST model: make the id optional
        """
        logger.debug(f"Generating {cls.__name__}__POST")
        model = pd.create_model(f"{cls.__name__}__POST", __base__=(cls, _Generated))

        model.model_fields.pop("meta")

        for field_name, field in model.model_fields.items():
            if field_name == 'id':
                field.default = None
                field.default_factory = None

        model.model_rebuild(force=True)

        return model

    @classmethod
    def _generate_filter_model(cls) -> Type[pd.BaseModel]:
        """
        Create a model for filtering purposes: make all fields optional, remove defaults
        """
        logger.debug(f"Generating {cls.__name__}__FILTER")
        atts = {}
        for field_name, field in cls.model_fields.items():
            field_info = copy(field)
            field_info.default = None
            field_info.default_factory = None
            atts[field_name] = (field.annotation, field_info)

        model = pd.create_model(f"{cls.__name__}__FILTER", __base__=(cls, _Generated), **atts)
        return model

    @staticmethod
    def _generate_includes_model() -> Type[pd.BaseModel]:
        """Return a list of Included JsonAPIRecord models"""
        if BaseModel._includes_model is None:
            logger.debug(f"Generating includes_model")

            all_models = []
            for model in BaseModel.__MODEL_REGISTRY__.values():
                all_models.append(model._generate_jsonapi_record_model())
            included_models = tuple(all_models)
            included_root = pd.create_model(f'JsonAPIInclude', **{
                'root': (Union[included_models], pd.Field(..., discriminator='type'))
            })

            BaseModel._includes_model = included_root

        return BaseModel._includes_model

    @classmethod
    def jsonapi_envelope_schema(cls, many: bool = False, use_ref_schema: bool = False):
        model_name = cls._get_envelope_schema_name(many, use_ref_schema)

        try:
            return BaseModel._jsonapi_schemas[model_name]
        except KeyError as e:
            raise Exception(f"Cannot find schema {model_name}, was BaseModel.jsonapi_init_schemas() called?") from e

    @classmethod
    def _get_envelope_schema_name(cls, many: bool = False, use_ref_schema: bool = False):
        if use_ref_schema:
            model_name = f"{cls.__name__}__RefEnvelope"
        else:
            model_name = f"{cls.__name__}__Envelope"

        if many:
            return model_name + 'Many'
        else:
            return model_name

    @classmethod
    def _generate_jsonapi_envelope_schema(cls, many: bool = False, use_ref_schema: bool = False) -> tuple[str, dict]:
        model_name = cls._get_envelope_schema_name(many, use_ref_schema)

        logger.debug(f"Generating {model_name}")

        if use_ref_schema:
            data_ref = f"#/components/schemas/{cls.__name__}Link"
        else:
            data_ref = f"#/components/schemas/{cls.__name__}"

        if many:
            data = {
                'items': {'$ref': data_ref},
                'title': 'Data',
                'type': 'array'
            }
        else:
            data = {'$ref': data_ref}

        envelope = {
            'properties': {
                'data': data,
                'included': {
                    'anyOf': [
                        {'items': {'$ref': '#/components/schemas/JsonAPIInclude'}, 'type': 'array'},
                        {'type': 'null'}
                    ],
                    'default': None,
                    'title': 'Included'
                }
            },
            'required': ['data'],
            'title': model_name,
            'type': 'object'
        }

        return model_name, envelope

    @classmethod
    def _initialize(cls, **kwargs):
        if BaseModel._initialized == True:
            raise Exception("Already initialized")

        logger.debug(f"Initializing {cls.__name__}")
        BaseModel.__MODEL_REGISTRY__[cls.__name__] = cls

        # init relationships
        cls.__JSONAPI_RELATIONSHIP_DEFS__ = {}
        relationships_class = getattr(cls, 'Relationships', None)
        relationships = getattr(relationships_class, '_relationships', []) if relationships_class else []
        for relationship in relationships:
            relationship.register_owner(cls)
            cls.__JSONAPI_RELATIONSHIP_DEFS__[relationship.relation_name] = relationship

        # init derived models
        cls._jsonapi_patch_model = cls._generate_patch_model()
        cls._jsonapi_post_model = cls._generate_post_model()
        cls.__jsonapi_filter_model__ = cls._generate_filter_model()
        cls.__jsonapi_record_link_model__, cls.__jsonapi_record_link_many_model__ = cls._generate_jsonapi_record_link_models()

        # postpone: these need to be lazy
        # cls._generate_jsonapi_record_model()
        # cls._generate_openapi_schema_definitions()


    @classmethod
    def validate_id(cls, id: str):
        """
        Validates the id by initiating a patch model.
        """
        cls._jsonapi_patch_model(id=id)

    @classmethod
    def jsonapi_from_record(cls, record: JsonAPIRecord, serialization_mode: SerializationMode = SerializationMode.DEFAULT):
        """
        Deserialize a record from JsonAPI format
        """
        vals = record.model_dump(exclude_unset=True)
        vals.pop('meta', None)

        # move attributes to the base level
        atts = vals.pop('attributes', {})
        vals.update(**atts)

        type_ = vals.pop('type')
        if type_ != cls.__name__:
            raise UnexpectedType(expected=cls.__name__, actual=type_)

        match serialization_mode:
            case SerializationMode.DEFAULT:
                return cls.model_validate(vals)
            case SerializationMode.PATCH:
                return cls._jsonapi_patch_model.model_validate(vals)
            case SerializationMode.POST:
                return cls._jsonapi_post_model.model_validate(vals)
            case _:
                raise ValueError

    def jsonapi_to_record(self, root_url: str) -> JsonAPIRecord:
        """
        Serialize model to JsonAPI record

        Attributes are moved to the correct Attributes embedded model; id and meta attributes are left in place.
        """
        atts = self.model_dump(exclude_none=True)
        relationships = {}
        meta = atts.pop('meta', {})
        relationships_meta = meta.pop('_relationships', {})
        self_id = atts.pop('id')
        self_url = f"{root_url}/{self_id}"

        for relation_name, relation_cfg in self.__JSONAPI_RELATIONSHIP_DEFS__.items():
            relationship = {
                'links': {
                    'related': f"{self_url}/{relation_name}",
                    'self': f"{self_url}/relationships/{relation_name}"
                },

            }

            relationship_meta = relationships_meta.pop(relation_name, {})

            # only include data element if this relation was actually included
            relation_att = self._relationships.get(relation_name, None)
            relation_count = self._relationship_counts.get(relation_name, 0)
            if relation_att is not None:
                # these relations may not be unique, so we make them unique first
                relation_refs = {ref: None for ref in relation_att}

                if relation_cfg.many:
                    # "many" relationship, always return list
                    relationship['data'] = [{'type': relation_cfg.related_model_name, 'id': relation_ref} for relation_ref in relation_refs]
                    relationship_meta.update({'count': relation_count})
                elif relation_refs:
                    # "one" relationship, return single ref if present
                    if len(relation_refs) != 1:
                        raise RuntimeError(f"Unexpected length {len(relation_refs)} for {relation_cfg.full_name}; should this be a to-many relationship?!")
                    relationship['data'] = {'type': relation_cfg.related_model_name, 'id': next(iter(relation_refs.keys()))}
                else:
                    # "one" relationship, but no data
                    relationship['data'] = None

            if relationship_meta:
                relationship['meta'] = relationship_meta

            relationships[relation_name] = relationship

        vals = {
            'id': str(self_id),
            'type': type(self).__name__,
            'links': {
                'self': self_url
            }
        }

        # only add attributes, relationships, meta if present
        if atts:
            vals['attributes'] = atts
        if relationships:
            vals['relationships'] = relationships
        if meta:
            vals['meta'] = meta

        return BaseModel._jsonapi_record_models[type(self).__name__].model_validate(vals)

    @staticmethod
    def jsonapi_init_schemas():
        """Initialize JsonAPI schemas"""
        if BaseModel._initialized == True:
            logger.debug("Already initialized, skipping")
            return

        # collect all subclasses, and call initialization
        def _get_subclasses(model_class: Type[BaseModel]):
            subclasses = model_class.__subclasses__()
            for subclass in subclasses:
                subclasses.extend(_get_subclasses(subclass))
            return subclasses

        logger.debug("Initializing jsonapi_init_schemas")

        subclasses = _get_subclasses(BaseModel)

        # Step 1: generate basic schemas
        for subclass in subclasses:
            subclass._initialize()

        logger.debug(f"Step 1: generate basic schemas DONE")

        # Step 2: generate JsonAPI record schemas
        for subclass in subclasses:
            subclass._generate_jsonapi_record_model()

        logger.debug(f"Step 2: generate JsonAPI record schemas DONE")

        # Step 3: generate model for includes
        includes_model = BaseModel._generate_includes_model()

        # generate OpenAPI schema
        includes_schema = includes_model.schema(by_alias=True, ref_template=REF_TEMPLATE)
        definitions = includes_schema.pop('$defs')
        definitions[includes_schema['title']] = includes_schema

        logger.debug(f"Step 3: generate JsonAPI schema for includes DONE")

        # Step 4: generate OpenAPI schemas
        variants = (
            (False, False),
            (False, True),
            (True, False),
            (True, True),
        )
        for subclass in subclasses:
            for variant in variants:
                model_name, schema = subclass._generate_jsonapi_envelope_schema(*variant)
                BaseModel._jsonapi_schemas[model_name] = schema

        BaseModel._jsonapi_schema_definitions.update(definitions)

        logger.debug(f"Step 4: generate OpenAPI schemas DONE")

        BaseModel._initialized = True


