from typing import Iterable, Type, Optional, Literal, Any, TypeVar, Generic, Annotated

import pydantic as pd


class JsonAPILinks(pd.RootModel):
    root: dict[str, str]


class JsonAPIRecordLink(pd.BaseModel):
    type: str
    id: str


class JsonAPIRelationship(pd.BaseModel):
    links: Optional[JsonAPILinks] = None
    data: Optional[JsonAPIRecordLink] | Optional[list[JsonAPIRecordLink]] = None
    meta: Optional[dict] = None


class JsonAPIRelationships(pd.RootModel):
    root: dict[str, JsonAPIRelationship]


class JsonAPIRecord(JsonAPIRecordLink):
    type: str
    id: Optional[str] = None
    attributes: Optional[dict] = None
    relationships: Optional[JsonAPIRelationships] = None
    links: Optional[JsonAPILinks] = None
    meta: Optional[dict] = None


class JsonAPIInfo(pd.BaseModel):
    version: str


class JsonAPIError(pd.BaseModel):
    status: str
    code: str
    title: str
    detail: str
    meta: Optional[dict] = None


class JsonAPIEnvelope(pd.BaseModel):
    data: Optional[JsonAPIRecord] = None
    included: Optional[list[JsonAPIRecord]] = None
    meta: Optional[dict] = None
    links: Optional[JsonAPILinks] = None
    jsonapi: Optional[JsonAPIInfo] = None


class JsonAPIEnvelopeMany(JsonAPIEnvelope):
    data: list[JsonAPIRecord]


class JsonAPIEnvelopeError(pd.BaseModel):
    errors: list[JsonAPIError]
    meta: Optional[dict] = None
    links: Optional[JsonAPILinks] = None
    jsonapi: Optional[JsonAPIInfo] = None


