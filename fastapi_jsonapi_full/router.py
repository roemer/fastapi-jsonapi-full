from typing import (
    Annotated,
    Any,
    Callable,
    Coroutine,
    Dict,
    Generic,
    Iterable,
    Literal,
    Optional,
    Sequence,
    Type,
    TypeVar,
    Union, List
)
from enum import Enum
from abc import ABC, abstractmethod
from functools import partial


import pydantic as pd
import pydantic.schema as pd_schema

from fastapi import Query, Depends, Request, Response, APIRouter, FastAPI, Path

from .schema import (
    JsonAPIEnvelope,
    JsonAPIEnvelopeMany,
    JsonAPIEnvelopeError,
    JsonAPIInfo,
    JsonAPILinks,
    JsonAPIRecord,
    JsonAPIRecordLink,
    JsonAPIRelationship,
    # __RESOURCE_REGISTRY__,
    # __RELATIONSHIP_REGISTRY__,
    # __MODEL_REGISTRY__
)
from .model import (
    BaseModel,
    SerializationMode,
    Relationship
)

from .data_layer import BaseDataLayer
from .params import DefaultPaginationParam
from .response import JsonAPIResponse
from .route import JsonAPIRoute

import fastapi_jsonapi_full.requests as requests
import fastapi_jsonapi_full.exceptions as exceptions
import fastapi_jsonapi_full.handlers as functions

import logging

logger = logging.getLogger(__name__)


class JsonAPIRouter(APIRouter):
    __RESOURCE_REGISTRY__: dict[str, 'JsonAPIRouter'] = {}
    """Registry of all resource managers"""

    model: Type[BaseModel]
    data_layer: Type[BaseDataLayer] | BaseDataLayer

    path: str
    name: str

    def __init__(self, path: str, model: Type[BaseModel], data_layer: BaseDataLayer, name: str = None, **kwargs):
        # logger.debug(f"Initializing resource {self.name}")
        super().__init__(route_class=JsonAPIRoute, **kwargs)

        self.model = model
        self.data_layer = data_layer

        self.path = f"{self.prefix}/{path}"
        self.name = name or model.__name__

        JsonAPIRouter.__RESOURCE_REGISTRY__[self.model.__name__] = self

    def add_jsonapi_delete_route(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.DeleteRequest,
            **kwargs
    ):
        func = partial(
            endpoint,
            request=Depends(request_class(self))
        )
        description = f"Delete '{self.model.__name__}'"
        self.add_api_route(
            f"{self.path}/{{id}}",
            func,
            methods=['delete'],
            responses=self.get_jsonapi_response(many=False),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            name=f"{self.name}_delete",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_get_many_route(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.GetManyRequest,
            **kwargs
    ):
        func = partial(
            endpoint,
            request=Depends(request_class(self))
        )
        description = f"Get multiple '{self.model.__name__}'"
        self.add_api_route(
            f"{self.path}",
            func,
            responses=self.get_jsonapi_response(many=True),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            name=f"{self.name}_get_many",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_get_route(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.GetRequest,
            **kwargs
    ):
        func = partial(
            endpoint,
            request=Depends(request_class(self))
        )
        description = f"Get one '{self.model.__name__}'"
        self.add_api_route(
            f"{self.path}/{{id}}",
            func,
            responses=self.get_jsonapi_response(many=False),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            name=f"{self.name}_get",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_relationship_delete_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipDeleteRequest,
            **kwargs
    ):
        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_relationship_delete_route(
                endpoint,
                relation_name,
                tags=tags,
                dependencies=dependencies,
                deprecated=deprecated,
                include_in_schema=include_in_schema,
                request_class=request_class,
                ** kwargs
            )

    def add_jsonapi_relationship_delete_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipDeleteRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Delete relationships for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/relationships/{relation_name}",
            func,
            methods=['DELETE'],
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(many=False, use_ref_schema=True),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': relation.related_model.jsonapi_envelope_schema(many=False,
                                                                                     use_ref_schema=True)
                        }
                    },
                    "required": True,
                }
            },
            name=f"{self.name}_relationship_{relation_name}_delete",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_related_post_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelatedPostRequest,
            **kwargs):

        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_related_post_route(
                endpoint,
                relation_name,
                tags=tags,
                dependencies=dependencies,
                deprecated=deprecated,
                include_in_schema=include_in_schema,
                request_class=request_class,
                **kwargs
            )

    def add_jsonapi_related_post_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelatedPostRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Create a related item for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/{relation_name}",
            func,
            methods=['POST'],
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(many=False, use_ref_schema=False),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': relation.related_model.jsonapi_envelope_schema(many=False, use_ref_schema=False)
                        }
                    },
                    "required": True,
                }
            },
            name=f"{self.name}_related_{relation_name}_post",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_relationship_post_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipPostRequest,
            **kwargs
    ):
        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_relationship_post_route(
                    self,
                    endpoint,
                    relation_name,
                    tags=tags,
                    dependencies=dependencies,
                    deprecated=deprecated,
                    include_in_schema=include_in_schema,
                    request_class=request_class,
                    **kwargs
            )

    def add_jsonapi_relationship_post_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipPostRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Create a relationship for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/relationships/{relation_name}",
            func,
            methods=['POST'],
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(many=False, use_ref_schema=True),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': relation.related_model.jsonapi_envelope_schema(many=False,
                                                                                     use_ref_schema=True)
                        }
                    },
                    "required": True,
                }
            },
            name=f"{self.name}_relationship_{relation_name}_post",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_relationship_patch_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipPatchRequest,
            **kwargs
    ):
        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_relationship_patch_route(
                endpoint,
                relation_name,
                tags=tags,
                dependencies=dependencies,
                deprecated=deprecated,
                include_in_schema=include_in_schema,
                request_class=request_class,
                **kwargs
            )

    def add_jsonapi_relationship_patch_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipPatchRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Update relationships for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/relationships/{relation_name}",
            func,
            methods=['PATCH'],
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(many=relation.many,
                                                                                                   use_ref_schema=True),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': relation.related_model.jsonapi_envelope_schema(many=relation.many,
                                                                                     use_ref_schema=True)
                        }
                    },
                    "required": True,
                }
            },
            name=f"{self.name}_relationship_{relation_name}_patch",

            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_related_get_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelatedGetRequest,
            **kwargs
    ):
        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_related_get_route(
                endpoint,
                relation_name,
                tags=tags,
                dependencies=dependencies,
                deprecated=deprecated,
                include_in_schema=include_in_schema,
                request_class=request_class,
                **kwargs
            )

    def add_jsonapi_related_get_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelatedGetRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Get related items for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/{relation_name}",
            func,
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(many=relation.many),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            name=f"{self.name}_related_{relation_name}_get",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_relationship_get_all_routes(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipGetRequest,
            **kwargs
    ):
        relation_names = self.model.__JSONAPI_RELATIONSHIP_DEFS__.keys()
        for relation_name in relation_names:
            self.add_jsonapi_relationship_get_route(
                endpoint,
                relation_name,
                tags=tags,
                dependencies=dependencies,
                deprecated=deprecated,
                include_in_schema=include_in_schema,
                request_class=request_class,
                **kwargs
            )

    def add_jsonapi_relationship_get_route(
            self,
            endpoint: Any,
            relation_name: str,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.RelationshipGetRequest,
            **kwargs
    ):
        relation = self.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        func = partial(
            endpoint,
            request=Depends(request_class(self, relation_name))
        )
        description = f"Get relationships for relationship '{relation.full_name}'"
        self.add_api_route(
            f"{self.path}/{{id}}/relationships/{relation_name}",
            func,
            responses=self.__RESOURCE_REGISTRY__[relation.related_model_name].get_jsonapi_response(
                many=relation.many,
                use_ref_schema=True
            ),
            response_class=JsonAPIResponse,
            description=description,
            summary=description,
            name=f"{self.name}_relationship_{relation_name}_get",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_post_route(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.PostRequest,
            **kwargs
    ):
        func = partial(
            endpoint,
            request=Depends(request_class(self))
        )
        description = f"Create '{self.model.__name__}'"
        self.add_api_route(
            f"{self.path}",
            func,
            methods=['post'],
            responses=self.get_jsonapi_response(many=False),
            response_class=JsonAPIResponse,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': self.model.jsonapi_envelope_schema(many=False)
                        }
                    },
                    "required": True,
                }
            },
            description=description,
            summary=description,
            name=f"{self.name}_post",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def add_jsonapi_patch_route(
            self,
            endpoint: Any,
            tags: Optional[list[str | Enum]] = None,
            dependencies: Optional[Sequence[Depends]] = None,
            deprecated: Optional[bool] = None,
            include_in_schema: bool = True,
            request_class: Type[requests.BaseRequest] = requests.PatchRequest,
            **kwargs
    ):
        func = partial(
            endpoint,
            request=Depends(request_class(self))
        )
        description = f"Update '{self.model.__name__}'"
        self.add_api_route(
            f"{self.path}/{{id}}",
            func,
            methods=['patch'],
            responses=self.get_jsonapi_response(many=False),
            response_class=JsonAPIResponse,
            openapi_extra={
                "requestBody": {
                    "content": {
                        "application/json": None,
                        "application/vnd.api+json": {
                            'schema': self.model.jsonapi_envelope_schema(many=False)
                        }
                    },
                    "required": True,
                }
            },
            description=description,
            summary=description,
            name=f"{self.name}_patch",
            # standard route arguments
            tags=tags, dependencies=dependencies, deprecated=deprecated, include_in_schema=include_in_schema,
            **kwargs
        )

    def get_jsonapi_response(self, many: bool = False, status_code: int = 200, use_ref_schema: bool = False):
        return {
            str(status_code): {
                "description": "Successful Response",
                'content': {
                    "application/vnd.api+json": {
                        'schema': self.model.jsonapi_envelope_schema(many=many, use_ref_schema=use_ref_schema)
                    },
                },
            },
        }


def register_jsonapi_schema_components(openapi_schema: dict):
    # get components
    schemas = openapi_schema['components']['schemas']
    # for model in BaseModel.__MODEL_REGISTRY__.values():
    #     # base schemas
    #     definitions = model.jsonapi_envelope_schema()[1]
    #     for name, definition in definitions.items():
    #         schemas[name] = definition
    #     # ref schemas
    #     definitions = model.jsonapi_envelope_schema(use_ref_schema=True)[1]
    #     for name, definition in definitions.items():
    #         schemas[name] = definition
    schemas.update(BaseModel._jsonapi_schema_definitions)
    # sort dict
    openapi_schema['components']['schemas'] = {name: schemas[name] for name in sorted(schemas)}

    # for path, path_cfg in openapi_schema['paths'].items():
    #     for method, method_cfg in path_cfg.items():
    #         logger.debug(method)
    #         parameters = [param for param in method_cfg['parameters'] if param['name'] != 'kwargs']
    #         method_cfg['parameters'] = parameters

