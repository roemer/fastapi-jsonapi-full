from typing import Type, Any, Tuple

from pydantic.fields import FieldInfo
import fastapi_jsonapi_full.exceptions as exceptions

from .model import BaseModel, Relationship


def find_field_by_name(model: Type[BaseModel], field_name: str) -> tuple[str, FieldInfo]:
    """Find a field by name or alias"""
    for field, field_cfg in model.model_fields.items():
        if field_name == field or field_name == field_cfg.alias:
            return field, field_cfg

    raise exceptions.UnprocessableContent(f"Field '{field_name}' unknown on model '{model.__name__}'")


def find_related_model(model: Type[BaseModel], relationship_path: list[str], to_many: bool) -> Type[BaseModel]:
    current_model = model

    for entity in relationship_path:
        try:
            child_model = current_model.__JSONAPI_RELATIONSHIP_DEFS__[entity]

            if child_model.many and not to_many:
                raise exceptions.UnprocessableContent(f"Cannot use to-many relationship '{entity}' directly. Use 'any' operation for filtering")
            if not child_model.many and to_many:
                raise exceptions.UnprocessableContent(f"Aggregate operation 'any' must use to-many relation while '{entity}' is to-one")

            current_model = child_model.related_model
        except KeyError:
            raise exceptions.UnprocessableContent(f"Cannot find relationship '{entity}' for model '{current_model}'")

    return current_model


async def append_relationship(
        resource_mgr: 'BaseResource',
        item: object,
        related_id: str,
        relation: Relationship,
        related_resource_mgr: 'BaseResource',
        **kwargs
) -> object:
    related_objects = await find_items_by_ids(
        related_resource_mgr,
        [related_id],
        relation_name=relation.full_name,
        **kwargs
    )
    related_object = related_objects[0]

    await resource_mgr.data_layer.append_related_objects(item, relation, related_object, **kwargs)
    return related_object


async def get_related_objects_sorted(
        resource_mgr,
        relationships: dict[str, list],
        **kwargs
) -> dict[Relationship, list[object]]:

    relation_name_mapped_related_objects_sorted: dict[Relationship, list[object]] = {}

    for relation_name, related_ids in relationships.items():
        # retrieve related objects
        relation = resource_mgr.model.__JSONAPI_RELATIONSHIP_DEFS__[relation_name]
        related_resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[relation.related_model_name]

        related_objects_sorted = await find_items_by_ids(
            related_resource_mgr,
            related_ids,
            relation_name=relation.full_name,
            **kwargs
        )

        relation_name_mapped_related_objects_sorted[relation] = related_objects_sorted

    return relation_name_mapped_related_objects_sorted


async def update_relationships(
        resource_mgr,
        item,
        relation_mapped_related_objects_sorted: dict[Relationship, list[object]],
        **kwargs
):
    # retrieve relationships
    for relation, related_objects_sorted in relation_mapped_related_objects_sorted.items():
        if relation.many:
            # to-many relationship
            await resource_mgr.data_layer.update_related_objects(item, relation, related_objects_sorted, **kwargs)
        elif related_objects_sorted:
            # to-one relationship with data
            await resource_mgr.data_layer.update_related_objects(item, relation, related_objects_sorted[0], **kwargs)
        else:
            # to-one relationship, no data
            await resource_mgr.data_layer.update_related_objects(item, relation, None, **kwargs)


async def create_item(
        resource_mgr,
        item_model,
        relationships: dict[Any, list],
        **kwargs
) -> object:

    relation_mapped_related_objects_sorted = await get_related_objects_sorted(resource_mgr, relationships, **kwargs)

    # self.before_create(item_model, relation_mapped_related_objects_sorted, **kwargs)

    # create new item & apply changes
    item = await resource_mgr.data_layer.create_orm(**kwargs)
    await resource_mgr.data_layer.update_orm(item, item_model, patch=False, **kwargs)

    await update_relationships(resource_mgr, item, relation_mapped_related_objects_sorted, **kwargs)

    # self.before_save(item, **kwargs)
    await resource_mgr.data_layer.save(**kwargs)

    return item


async def find_item_by_id(resource_mgr, item_id: str, **kwargs) -> object:
    """
    Find item by id

    Return exactly one result; raises ItemNotFound if the id could not be found
    """
    resource_mgr.model.validate_id(item_id)

    data = await resource_mgr.data_layer.find_by_ids([item_id], **kwargs)
    it = iter(data)
    item = next(it, None)
    if not item:
        raise exceptions.ItemNotFound(item_id)
    elif next(it, None):
        raise exceptions.TooManyResults()
    else:
        return item


async def find_items_by_ids(resource_mgr, ids: list[str], relation_name: str = None, **kwargs) -> list[object]:
    """
    Find items by ids

    Return ordered list of results for each id; raises ItemNotFound if an item could not be found.
    """

    for id in ids:
        resource_mgr.model.validate_id(id)

    items = await resource_mgr.data_layer.find_by_ids(ids, **kwargs)

    items_by_id = {}
    for item in items:
        item_id = resource_mgr.data_layer.get_id(item)
        if item_id in items_by_id:
            raise exceptions.TooManyResults(f"Item {item_id} was already returned, results must be unique.")
        items_by_id[item_id] = item

    items_sorted = []
    for item_id in ids:
        try:
            items_sorted.append(items_by_id[item_id])
        except KeyError:
            raise exceptions.ItemNotFound(item_id=item_id, relationship=relation_name)

    return items_sorted


async def get_related_items(
        root_item,
        relation: Relationship,
        related_resource_mgr: 'BaseResource',
        pagination: 'DefaultPaginationParam',
        **kwargs
) -> Tuple[list[Any], int]:
    # get related objects through FK
    related_items = []
    count = 0
    fk = getattr(root_item, relation.foreign_key)
    if fk is not None:
        results_by_fk = await related_resource_mgr.data_layer.find_related(
            foreign_keys=[fk],
            relation_config=relation,
            pagination=pagination,
            **kwargs
        )

        # there must be a single result: that for the FK
        related_items, count = results_by_fk[fk]

    return related_items, count


async def process_included(
        resource_mgr: 'BaseResource',
        data: list[object],
        registry: dict,
        included_relation_paths: dict,
        **kwargs
):
    base_registry = registry[resource_mgr.model.__name__]

    for relation_name, (deeper_included_relations, limit) in included_relation_paths.items():
        relation = Relationship.__RELATIONSHIP_REGISTRY__[relation_name]
        relation_model = relation.related_model
        resource_mgr = resource_mgr.__RESOURCE_REGISTRY__[relation.related_model_name]

        # get foreign keys; order data in dictionary based on the foreign key for easy assignment afterwards
        data_by_fk = {}
        for o in data:
            model = base_registry[resource_mgr.data_layer.get_id(o)]
            # initialize relation
            model._relationships.setdefault(relation.relation_name, [])
            model._relationship_counts.setdefault(relation.relation_name, 0)

            fk = getattr(o, relation.foreign_key)
            if fk is None:
                continue

            data_by_fk.setdefault(fk, []).append(model)

        # call data layer to retrieve the related objects,
        # and merge the result with the registry
        kwargs['limit'] = limit
        kwargs['pagination'] = None
        registry_entries = registry.setdefault(relation.related_model_name, {})
        results_by_fk = await resource_mgr.data_layer.find_related(
            foreign_keys=data_by_fk.keys(),
            relation_config=relation,
            **kwargs
        )
        related_objects = {}

        for fk, (related_objects_list, related_objects_count) in results_by_fk.items():
            for related_object in related_objects_list:
                related_object_id = resource_mgr.data_layer.get_id(related_object)
                # try lookup in the registry
                related_model = registry_entries.get(related_object_id)
                if not related_model:
                    related_model = relation_model.from_orm(related_object)
                    registry_entries[related_object_id] = related_model

                related_objects.setdefault(related_object_id, related_object)

                for model in data_by_fk[fk]:
                    model._relationships[relation.relation_name].append(related_object_id)
                    model._relationship_counts[relation.relation_name] = related_objects_count

        # now recurse if there are deeper includes
        await process_included(resource_mgr, related_objects.values(), registry, deeper_included_relations, **kwargs)


async def remove_relationship(
        resource_mgr: 'BaseResource',
        item: object,
        related_ids: list[str],
        relation_cfg: Relationship,
        relation_resource_mgr: 'BaseResource',
        **kwargs
):
    related_objects = await find_items_by_ids(
        relation_resource_mgr,
        related_ids,
        relation_name=relation_cfg.full_name,
        **kwargs
    )
    await resource_mgr.data_layer.remove_related_objects(item, relation_cfg, related_objects, **kwargs)


async def update_relationship(
        resource_mgr,
        item: object,
        related_ids: list[str] | None,
        relation_cfg: Relationship,
        relation_resource_mgr: 'BaseResource',
        **kwargs
) -> list[object]:
    related_ids = related_ids or []

    related_objects = await find_items_by_ids(
        relation_resource_mgr,
        related_ids,
        relation_name=relation_cfg.full_name,
        **kwargs
    ) if related_ids else []

    if relation_cfg.many:
        # to-many relation: always use list
        relationship_data = related_objects
    elif related_objects:
        # to-one relation with data
        relationship_data = related_objects[0]
    else:
        relationship_data = None

    await resource_mgr.data_layer.update_related_objects(item, relation_cfg, relationship_data, **kwargs)

    return related_objects


