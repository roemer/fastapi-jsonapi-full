import json
from json import JSONDecodeError
from typing import Annotated, Type, Any, Optional, Literal, Union, List
from fastapi import Query, Request
import pydantic as pd
from dataclasses import dataclass

from ..model import BaseModel, Relationship
from ..utils import find_field_by_name, find_related_model

from fastapi_jsonapi_full.utils import exceptions

FILTER_PREFIX = 'filter['
FILTER_SUFFIX = ']'


class NotFilterOp(pd.BaseModel):
    op: Literal['not'] = 'not'
    val: 'FilterOp'


class AndFilterOp(pd.BaseModel):
    op: Literal['and'] = 'and'
    val: list['FilterOp']


class OrFilterOp(pd.BaseModel):
    op: Literal['or'] = 'or'
    val: list['FilterOp']


class AnyFilterOp(pd.BaseModel):
    op: Literal['any'] = 'any'
    val: Union[list['FilterOp'], 'FilterOp']
    name: str


class FieldValueFilterOp(pd.BaseModel):
    op: Optional[str] = pd.Field('eq')
    val: list[str] | str | list[int] | int | list[float] | float | bool | None = pd.Field(..., union_mode='left_to_right')
    name: str


def get_discriminator_value(v: Any) -> str:
    if isinstance(v, dict):
        tag = v.get('op')
    else:
        tag = getattr(v, 'op', None)

    if tag in ['and', 'or', 'not', 'any']:
        return tag
    else:
        return 'op'


FilterOp = Annotated[
    Union[
        Annotated[NotFilterOp, pd.Tag('not')],
        Annotated[AndFilterOp, pd.Tag('and')],
        Annotated[OrFilterOp, pd.Tag('or')],
        Annotated[AnyFilterOp, pd.Tag('any')],
        Annotated[FieldValueFilterOp, pd.Tag('op')],
    ], pd.Discriminator(get_discriminator_value)
]

# Resolve forward references
NotFilterOp.model_rebuild()
AndFilterOp.model_rebuild()
OrFilterOp.model_rebuild()
AnyFilterOp.model_rebuild()


class FilterOps(pd.RootModel):
    root: list[FilterOp]


@dataclass
class FilterClause:
    op: str
    val: Optional[Any] = None
    filters: Optional[List['FilterClause']] = None
    filter: Optional['FilterClause'] = None
    field_name: Optional[str] = None
    field: Optional[pd.Field] = None
    # relation: Optional[Type[Relationship]] = None
    model: Optional[Type[BaseModel]] = None
    path: Optional[str] = None


_filter_description = """
Filter criteria. Examples:

 - Equality: `{"name": "firstName", "val": "Max"}`
 - Equality, explicit: `{"name": "firstName", "op": "eq", "val": "Max"}`
 - In: `{"name": "firstName", "op": "in", "val": ["Max", "John"]}`
 - Between: `{"name": "birthDate", "op": "between", "val": ["1990-01-01", "2000-01-01"]}`
 - And: `[{"name": "birthDate", "op": "ge", "val": "1990-01-01"},{"name": "birthDate", "op": "lt", "val": "2000-01-01"}]`
 - And, explicit: `{"op": "and", "val": [{"name": "birthDate", "op": "ge", "val": "1990-01-01"}, {"name": "birthDate", "op": "lt", "val": "2000-01-01"}]}`

A shortcut bracket notation may be used for simple filters: `...?filter[firstName]=Max`

The following operators are supported:

 - **and**: AND-operator
 - **or**: OR-operator
 - **not**: NOT-operator
 - **eq**: check if field is equal to something
 - **ieq**: check if field is equal to something (case insensitive)
 - **ne**: check if field is not equal to something
 - **gt**: check if field is greater than to something
 - **ge**: check if field is greater than or equal to something
 - **lt**: check if field is less than to something
 - **le**: check if field is less than or equal to something
 - **between**: used to filter a field between two values
 - **in**: check if field is in a list of values
 - **endswith**: check if field ends with a string
 - **ilike**: check if field contains a string (case insensitive)
 - **is**: check if field is a value
 - **isnot**: check if field is not a value
 - **like**: check if field contains a string
 - **match**: check if field match against a string or pattern
 - **notilike**: check if field does not contain a string (case insensitive)
 - **notin**: check if field is not in a list of values
 - **notlike**: check if field does not contain a string
 - **startswith**: check if field starts with a string
 - **any**: check if a any to-many relation exists with given filters

Entities **may** define filtering on *related entities* using the '.' notation, eg. `{"name": "organization-members.firstName", "val": "Max"}`.

"""


class FilterParam:
    """Filter parameter handling"""

    filter: FilterOp = None

    def __init__(self, request: Request, filter: Annotated[str | None, Query(description=_filter_description)] = None):
        # walk through request parameters, match filters
        try:
            filters = []
            if filter:
                filter_args = json.loads(filter)
                if not isinstance(filter_args, list):
                    filter_args = [filter_args]

                filters = FilterOps.model_validate(filter_args).root
        except JSONDecodeError as jde:
            raise exceptions.UnprocessableContent(detail=str(jde))

        for key, val in request.query_params.items():
            if key.startswith(FILTER_PREFIX) and key.endswith(FILTER_SUFFIX):
                field = key[len(FILTER_PREFIX):-len(FILTER_SUFFIX)]
                filters.append(FieldValueFilterOp(
                    op='eq',
                    val=val,
                    name=field
                ))

        if filters:
            if len(filters) == 1:
                self.filter = filters[0]
            else:
                self.filter = AndFilterOp(val=filters)

    def parse(self, model: Type[BaseModel]) -> FilterClause | None:
        """
        Create a FilterClause from the querystring parameters
        """
        if self.filter:
            return self._parse(self.filter, model)

        return None

    def _parse(self, filter: FilterOp, model: Type[BaseModel]) -> FilterClause:

        if isinstance(filter, AndFilterOp) or isinstance(filter, OrFilterOp):
            return FilterClause(
                op=filter.op,
                filters=[self._parse(subfilter, model) for subfilter in filter.val]
            )
        elif isinstance(filter, NotFilterOp):
            return FilterClause(
                op=filter.op,
                filter=self._parse(filter.val, model)
            )
        elif isinstance(filter, AnyFilterOp):
            resolved_model = find_related_model(model, [filter.name], to_many=True)
            if isinstance(filter.val, list):
                return FilterClause(
                    # relation=resolved_model,
                    path=filter.name,
                    op=filter.op,
                    filter=FilterClause(
                        op='and',
                        filters=[self._parse(subfilter, resolved_model) for subfilter in filter.val]
                    )
                )
            else:
                return FilterClause(
                    # relation=resolved_model,
                    path=filter.name,
                    op=filter.op,
                    filter=self._parse(filter.val, resolved_model)
                )

        parts = filter.name.split('.')
        entities = parts[0:-1]
        attribute = parts[-1]

        resolved_model = find_related_model(model, entities, to_many=False)
        resolved_field_name, resolved_field = find_field_by_name(resolved_model, attribute)

        return FilterClause(
            op=filter.op,
            val=filter.val,
            model=resolved_model,
            field_name=resolved_field_name,
            field=resolved_field,
            path='.'.join(entities)
        )


