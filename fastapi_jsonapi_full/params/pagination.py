from typing import Annotated, Tuple, Type, Any, Optional, Literal, Union, List
import math
from fastapi import Query, Request
from starlette.datastructures import MultiDict, QueryParams
from pydantic import Field


DEFAULT_PAGE_SIZE = 10


class DefaultPaginationParam:
    """Default pagination (page based) parameter handling"""

    limit: int = None
    """Page limit (size)"""

    offset: int = None
    """Page offset"""

    count: int = None
    """Total result count"""

    request: Request = None

    def __init__(
            self,
            request: Request = None,
            limit: Annotated[int | None, Query(alias="page[limit]", description='Page limit', ge=0)] = DEFAULT_PAGE_SIZE,
            offset: Annotated[int | None, Query(alias="page[offset]", description='Page offset', ge=0)] = 0,
    ):
        self.request = request
        self.limit = limit
        self.offset = offset

    def set_count(self, count: int):
        """Set total result count"""
        self.count = count

    def page_count(self) -> int:
        """Calculate the total number of pages"""
        return math.ceil(self.count / self.limit) if self.limit else 0

    def next_offset(self) -> int | None:
        no = self.offset + self.limit
        if no < self.count:
            return no
        else:
            return None

    def previous_offset(self) -> int | None:
        no = self.offset - self.limit
        if no >= 0:
            return no
        else:
            return None

    def last_offset(self) -> int | None:
        pc = self.page_count()
        if pc > 0:
            lo = (pc - 1) * self.limit
            return lo
        else:
            return None

    def first_offset(self) -> int | None:
        if self.page_count():
            return 0
        else:
            return None

    def rewrite_url(self, **replace_kwargs) -> str:
        """Rewrite querystring parameters"""
        md = MultiDict(self.request.query_params)
        md.update(replace_kwargs)
        query_params = QueryParams(md)
        return self.request.url.path + '?' + str(query_params)

    def next_url(self):
        no = self.next_offset()
        if no is not None:
            return self.rewrite_url(**{'page[offset]': no})
        else:
            return None

    def previous_url(self):
        po = self.previous_offset()
        if po is not None:
            return self.rewrite_url(**{'page[offset]': po})
        else:
            return None

    def first_url(self):
        fo = self.first_offset()
        if fo is not None:
            return self.rewrite_url(**{'page[offset]': fo})
        else:
            return None

    def last_url(self):
        lo = self.last_offset()
        if lo is not None:
            return self.rewrite_url(**{'page[offset]': lo})
        else:
            return None

    def get_links(self) -> dict[str, str]:
        """Get pagination links"""
        links = {
            'next': self.next_url(),
            'previous': self.previous_url(),
            'first': self.first_url(),
            'last': self.last_url(),
        }
        return {k: v for k, v in links.items() if v is not None}

    def get_meta(self) -> dict:
        """Get pagination meta information"""
        return {
            'count': self.count
        }

