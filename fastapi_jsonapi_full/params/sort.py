from typing import Annotated, Type
from fastapi import Query, Request
import pydantic as pd
from dataclasses import dataclass

from ..model import BaseModel
from ..utils import find_field_by_name, find_related_model


@dataclass
class SortClause:
    ascending: bool
    path: str
    field_name: str
    model: Type[BaseModel]


_sort_description = """
Sort criteria. Examples:
 
 - Normal, ascending: `firstName`
 - Normal, descending: `-birthDate`
 - Combined: `firstName,-birthDate`

 
Entities **may** define sorting on *related entities* using the '.' notation, eg. `name,organization-members.lastName`.
 
"""

class SortParam:
    """Sort parameter handling. """

    sort: dict[str, bool] = None
    """Sort"""

    def __init__(self, sort: Annotated[str | None, Query(description=_sort_description)] = None):
        self.sort = {}
        if sort:
            for field_sort in sort.split(','):
                ascending = True
                field = field_sort.strip().removeprefix('+')
                if field_sort.startswith('-'):
                    ascending = False
                    field = field_sort.removeprefix('-')
                self.sort[field] = ascending

    def parse(self, model: Type[BaseModel]) -> list[SortClause]:
        result = []

        for key, ascending in self.sort.items():
            parts = key.split('.')
            entities = parts[0:-1]
            attribute = parts[-1]

            resolved_model = find_related_model(model, entities, to_many=False)
            field_name, _ = find_field_by_name(resolved_model, attribute)
            path = '.'.join(entities) or None

            result.append(
                SortClause(
                    path=path,
                    field_name=field_name,
                    ascending=ascending,
                    model=resolved_model
                )
            )

        return result

    def __bool__(self):
        return len(self.sort) != 0