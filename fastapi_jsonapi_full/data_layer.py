from typing import (
    Annotated,
    Any,
    Callable,
    Coroutine,
    Dict,
    Generic,
    Iterable,
    Literal,
    Optional,
    Sequence,
    Type,
    TypeVar,
    Tuple
)
from abc import ABC, abstractmethod

from .schema import (
    JsonAPIEnvelope,
    JsonAPIEnvelopeMany,
    JsonAPIInfo,
    JsonAPILinks,
    JsonAPIRecord,
    JsonAPIRecordLink,
    JsonAPIRelationship,
)
from .model import (
    BaseModel,
    Relationship
)
from .exceptions import Forbidden, ItemNotFound


class BaseDataLayer(ABC):

    model: Type[BaseModel]
    """The main entity Model"""

    def __init__(self):
        assert self.model is not None

    async def create_orm(self, **kwargs) -> object:
        raise NotImplemented

    @abstractmethod
    async def find_by_ids(self, ids: Iterable[str], **kwargs) -> list[object]:
        # TODO: I do not like this construct
        # At least, the data layer should take care of ordered results
        raise NotImplemented

    @abstractmethod
    async def find_many(self, **kwargs) -> Tuple[list[object], int]:
        """
        Execute a list query; return a tuple with an Iterable and the total number of results.
        """
        raise NotImplemented

    @abstractmethod
    async def find_related(self, foreign_keys: Iterable[Any], relation_config: Relationship, **kwargs) \
            -> dict[Any, tuple[list[object], int]]:
        """
        Find related items through the foreign keys.

        This ResourceManager should return the items linked to the supplied foreign_keys.
        The calling ResourceManager supplies the foreign keys.

        :param foreign_keys: Iterable of foreign keys to look up
        :param relation_config: The relation to retrieve the related items for
        :param kwargs:
        :return: Iterable of (foreign_key, items), total result count (int)
        """
        raise NotImplemented

    async def update_related_objects(self, item: object, relationship: Relationship, relationship_data: object | list[object] | None, **kwargs):
        """
        Update related objects
        """
        att = getattr(item, relationship.attribute_name)
        if relationship.many:
            if isinstance(att, set):
                setattr(item, relationship.attribute_name, set(relationship_data))
            else:
                setattr(item, relationship.attribute_name, list(relationship_data))
        else:
            setattr(item, relationship.attribute_name, relationship_data)

    async def append_related_objects(self, item: object, relationship: Relationship, relationship_data: object, **kwargs):
        """
        Append related objects
        """
        att = getattr(item, relationship.attribute_name)
        if relationship.many:
            if isinstance(att, set):
                att.add(relationship_data)
            else:
                att.append(relationship_data)
        elif att is None:
            setattr(item, relationship.attribute_name, relationship_data)
        else:
            raise Forbidden("Relationship already defined")

    async def remove_related_objects(self, item: object, relationship: Relationship, related_objects: list[object], **kwargs):
        """
        Delete related objects
        """
        att = getattr(item, relationship.attribute_name)
        if relationship.many:
            for o in related_objects:
                att.remove(o)
        else:
            setattr(item, relationship.attribute_name, None)

    @abstractmethod
    async def save(self, **kwargs):
        """
        Save the current unit_of_work (transaction)
        """
        raise NotImplemented

    def get_id(self, item: object) -> str:
        """
        Return the id as a string for the given item
        """
        return str(item.id)

    async def update_orm(self, item: object, model: BaseModel, patch: bool = False, **kwargs):
        updates = model.model_dump(exclude_unset=patch)
        for field, value in updates.items():
            if field == 'id':
                # Only assign id when explicitly present
                continue

            setattr(item, field, value)

    async def delete_orm(self, item: object, **kwargs):
        raise NotImplemented
