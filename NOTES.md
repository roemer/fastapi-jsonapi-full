# Notes

Schema class defines:

 - own Schema fields
 - class Meta:
   - type_ = 'users'                        # Type of this Schema 
   - strict = True  
   - self_route = 'users:get'
   - self_route_kwargs = {'id': '<id>'}
   - self_route_many = 'users:get_many'

Resource class:
 - get
 - get_many
 - patch
 - post
 - put (optional, non-standard -> not needed!)

Resource class also returns data for 'included' calls:
 - top-level model is gathered first
 - then the related Resources are called with the top-level id as a kwarg
 - this is defined in the relation

Resource class methods are hooked to a FastAPI app (are a FastAPI app?!) by registering them as a router.
This is done using a decorator.
Decorator takes care of:
 - serialization into JsonAPI envelope
 - retrieval of related/ included objects
 - 

